#include <iostream>
#include <iomanip>
#include <fstream>
# define M_PI           3.14159265358979323846

using namespace std;

 int main(){

    setlocale(LC_ALL, "Lithuanian");
    double d, a;
    cout << "�veskite apelsino skersmen�: "; cin >> d;
    cout << "�veskite apelsino �ievel�s stor�: "; cin >> a;
    d /= 2;
    d -= a;
    double v = ((double)4 / 3) * M_PI * (d * d * d);
    cout << "Apelsino mink�timo t�ris: " << fixed << setprecision(2) << v;

 return 0; }

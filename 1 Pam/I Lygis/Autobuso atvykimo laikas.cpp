#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){
    setlocale(LC_ALL, "Lithuanian");
    int h, m, all = 0;
    cout << "Kada autobusas i�vyko i� Vilniaus? "; cin >> h >> m;
    all += h * 60 + m;
    cout << "Kiek laiko autobusas va�iavo i� Vilniaus � Panev���? "; cin >> h >> m;
    all += h * 60 + m;
    cout << "Kelias minutes autobusas stov�jo Panev��yje? "; cin >> m;
    all += m;
    cout << "Kiek laiko autobusas va�iavo i� Panev��io � Ryg�? "; cin >> h >> m;
    all += h * 60 + m;
    cout << "Autobusas � Ryg� atvyks: " << endl;
    all %= 1440;
    cout << all / 60 << " val. " << all % 60 << " min.";

 return 0; }

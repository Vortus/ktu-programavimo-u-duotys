#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

 int main(){

    setlocale(LC_ALL, "Lithuanian");
    double k, c;
    cout << "�veskite temperat�r� Kelvino skal�je: "; cin >> k;
    c =  k - 273.16;
    cout << fixed << setprecision(2) <<
            c << " Celcijaus laipsniu" << endl
         << c * 1.8 + 32 << " Farenheito laipsniu" << endl
         << c * 0.8 << " Reomiuro laipsniu";

 return 0; }

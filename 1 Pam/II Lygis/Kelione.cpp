#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

    int stotiKartu(double a, int b){ // kiek kartu reik sustot
        int cld = a / b + 0.5;
        return cld;
    }

    double truks(double a, int b, int c){ // kiek uztruks
        return ((a / b) * 60) + stotiKartu(a, c) * 5;
    }

 int main(){

    ifstream fi("Duomenys2.txt");
    int a, b, c, d, e; fi >> a >> b >> c >> d >> e; // is eiles nuskaitomi
    fi.close();

    int p = truks(b, c, a); // pirmo
    double an = truks(d, e, a); // antro

    /////////////Isvedimas
    ofstream fo("Rezultatai2.txt");
    int h2 = (int)an / 60;
    int min2 = (an - h2 * 60) + 0.5;
    cout << "Pirmu keliu: " << p / 60 << " h " << p % 60 << " min" << endl;
    cout << "Antru keliu: " << h2 << " h " << min2 << " min" << endl;
    if(p < an) cout << "Pirmu keliu grei�iau";
    else if(p > an) cout << "Antru keliu grei�iau";
    else cout << "Abiem keliais nesiskiria";
    fo.close();
    //////////////////////

 return 0; }

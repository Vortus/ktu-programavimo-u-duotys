#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    double islaidos(double autK, double vidkm, double kainaL){ // islaidos apskaiciavimas
        return autK + (36000 * vidkm * kainaL);
    }

    string geriau(int a, int b){ // kuris geresnis
        if(a < b) return "Geriau apsimoka pirkti benzinu varom� automobil�";
        return "Geriau apsimoka pirkti dyzeliu varom� automobil�";
    }

 int main(){

    ifstream fi("Duomenys1.txt");
    double bvid, dvid; fi >> bvid >> dvid; fi.close();
    bvid /= 100; dvid /= 100; // is 100 km i 1

    double a = islaidos(2500, bvid, 1.12); // benzinas
    double b = islaidos(3200, dvid, 0.99); // dyzelis

    ///////////Isvedimas
    ofstream fo("Rezultatai1.txt");
    fo << fixed << setprecision(2) << a << endl << b << endl << geriau(a, b);
    fo.close();
    /////////////////
 return 0; }

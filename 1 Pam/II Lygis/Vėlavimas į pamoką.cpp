#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

    int startMin; // pradines minutes

    int getLate(int a, int b){ // kiek veluoja
        return startMin - (a * 60 + b);
    }

 int main(){

    ofstream fo("Rezultatai3.txt");
    ifstream fi("Duomenys3.txt");
    int n, h, m, late = 0, allLate = 0; fi >> n >> h >> m;
    startMin = h * 60 + m;
    for(int i = 0; i < n; i++){
        fi >> h >> m;
        int j = abs(getLate(h, m)); // abs nes gaunas neigiamas jei veluoja aisku galejau i funkcija ikelt arba apsukt
        if(j > 0) late++;
        allLate += j;
        fo << j << endl; // velavimo laikas
    }
    fi.close();

    //////Isvedimas
    fo << late << " " << n - late << endl;
    fo << fixed << setprecision(1) <<
        (double)allLate / n << endl;

    if(n / 4 > late) fo << "GALIMA";
    else fo << "NEGALIMA";
    fo.close();
    /////////////////////

 return 0; }

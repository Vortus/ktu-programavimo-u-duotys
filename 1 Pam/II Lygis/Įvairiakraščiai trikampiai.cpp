#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

    double plotas(double a, int b, int c){ // ploto formule heronas
        double p = (a + b + c) / 2;
        return sqrt(p * (p - a) * (p - b) * (p - c));
    }

    bool arTrikampis(int a, int b, int c){ // ar trikampis
        if ((a + b > c) && (a + c > b) && (b + c > a)) return true;
        return false;
    }

    bool arIv(int a, int b, int c){ // ar ivairiakrastis
        if(a != b && b != c) return true;
        return false;
    }

 int main(){

    ofstream fo("Rezultatai4.txt");
    int a, b; cin >> a >> b;
    for(int i = a; i <= b; i++){ // ieskau per visas tris krastines
        for(int j = i + 1; j <= b; j++){
            for(int k = j + 1; k <= b; k++){
                if(arTrikampis(i, j, k) && arIv(i, j, k))
                    fo << i << " " << j << " " << k << " " << fixed
                        << setprecision(2) << plotas(i, k, j) << endl;
            }
        }
    }
    fo.close();

 return 0; }

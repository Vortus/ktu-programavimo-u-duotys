#include <iostream>
#include <iomanip>
#include <fstream>
#include <string.h>

using namespace std;

    char emailArr[256]; // globalus emailo simboliu masyvas
    int emailLength, minN, minD; // visas ilgis, min vardo ilgis, min domeno

    void readFile(){ // failo nuskaitymas
        ifstream fi("email.txt");
        fi >> minN >> minD; fi.ignore(); // \n praignorina skaitant
        fi.getline(emailArr, 256); // paiema eilute
        emailLength = strlen(emailArr); // gauna ilgi
        fi.close();
    }

    int getNameLength(){ // p dez pavadinimo ilgis
        int result = -1; // jei neranda -1

        for(int i = 0; i < emailLength; i++){
            if(emailArr[i] == '.') return result; // jei rado
            if(result >= 0) result++; //ilgis
            if(emailArr[i] == '@') result = 0; // rado pradzia
        }

        return result;
    }

    int getDOMLength(){ // domeno ilgis
        for(int i = emailLength - 1; i >= 0; i--) // einu nuo galo ir ieskau
            if(emailArr[i] == '.') return emailLength - i - 1; // - 1 nes su masyvais taip
        return -1; // jei nera domeno
    }

    bool goodSymbols(){ // tikrina simbolius ar geri
        for(int i = 0; i < emailLength; i++){
            int j = emailArr[i];
            if((j >= 65 && j <= 90) || (j >= 97 && j <= 122) || // jei netinka simboliai
                emailArr[i] == '.' || emailArr[i] == '@');
            else return false;
        }
        return true;
    }

    void checkEmail(){ // email tikrinimas
        if(getNameLength() < minN){ cout << "Email neteisingas." << endl << "Per trumpas pa�to d��ut�s pavadinimas."; return; }
        if(getDOMLength() < minD){ cout << "Email neteisingas." << endl << "Per trumpas domeno pavadinimas."; return; }
        if(!goodSymbols()){ cout << "Email neteisingas." << endl << "Yra neleid�iam� simboli�."; return; }
        cout << "Email teisingas!";
    }

 int main(){

    setlocale(LC_ALL, "Lithuanian"); // kad lt raides veiktu
    readFile();
    checkEmail();

 return 0; }

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string.h>
#include <vector>

using namespace std;

    struct Keyboard{

        int sizes[26] = { 1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3, 1,
                            2, 3, 1, 2, 3, 4, 1, 2, 3, 1, 2, 3, 4 };

        char values[26][4]{

            {'2', '0', '0', '0' }, //a
            {'2', '2', '0', '0' }, //b
            {'2', '2', '2', '0' }, //c

            {'3', '0', '0', '0' }, //d
            {'3', '3', '0', '0' }, //e
            {'3', '3', '3', '0' }, //f

            {'4', '0', '0', '0' }, //g
            {'4', '4', '0', '0' }, //h
            {'4', '4', '4', '0' }, //i

            {'5', '0', '0', '0' }, //j
            {'5', '5', '0', '0' }, //k
            {'5', '5', '5', '0' }, //l

            {'6', '0', '0', '0' }, //m
            {'6', '6', '0', '0' }, //n
            {'6', '6', '6', '0' }, //o

            {'7', '0', '0', '0' }, //p
            {'7', '7', '0', '0' }, //q
            {'7', '7', '7', '0' }, //r
            {'7', '7', '7', '7' }, //s

            {'8', '0', '0', '0' }, //t
            {'8', '8', '0', '0' }, //u
            {'8', '8', '8', '0' }, //v

            {'9', '0', '0', '0' }, //w
            {'9', '9', '0', '0' }, //x
            {'9', '9', '9', '0' }, //y
            {'9', '9', '9', '9' }, //y

        };
    };

    char sequence[1000]; // seka
    char decryptedSequence[1000]; // issifruota seka
    Keyboard keyboard;

    void readFile(){ //failo nuskaitymas
        ifstream fi("Duomenys4.txt");
        int tmp; fi >> tmp; fi.ignore();
        for(int i = 0; i < tmp; i++){
            char c; fi.get(c);
            if(c == '\n') i--; // jei nuskaito naujos eilutes simboli praleisti
            else sequence[i] = c;
        }
        fi.close();
    }

    int getChar(vector<char> v){ // gauti raides indeksui 97, galejau ciapat prid�t 97 bet man taip aiskiau

        for(int i = 0; i < 26; i++){
            bool b = true;
            for(int j = 0; j < keyboard.sizes[i]; j++){
                for(int k = 0; k < v.size(); k++){
                    if(v.at(k) != keyboard.values[i][j]){ b = false; break; }
                }
                if(!b) break;
            }
            if(b && v.size() == keyboard.sizes[i]) return i;
        }

        return -1;
    }

    void decrypt(){ // issifravimas

        int index = 0, sp = 0;
        char c = 'X';
        vector<char> tmpV; // laikinas

        while(c != '1'){
            c = sequence[index];
            if(c != '0'){ tmpV.push_back(c); sp = 0; } // nera nulio
            else {
                if(sp == 0){ decryptedSequence[strlen(decryptedSequence)] = (char)(97 + getChar(tmpV)); tmpV.clear(); } // vienas nulis
                else decryptedSequence[strlen(decryptedSequence)] = ' '; // tarpas kai buna du 0
                sp++;
            }
            index++;
        }

        if(tmpV.at(0) != '1') { // galo patikrinimas
                tmpV.pop_back(); // nuema vieneta is galo
                decryptedSequence[strlen(decryptedSequence)] = (char)(97 + getChar(tmpV)); // paskutine raide
        }
        decryptedSequence[strlen(decryptedSequence)] = '.'; // taskas lol
    }

    void returnToFile(){ // i faila
        ofstream fo("Rezultatai4.txt");
        fo << decryptedSequence;
        fo.close();
    }

 int main(){

    readFile();
    decrypt();
    returnToFile();

 return 0; }

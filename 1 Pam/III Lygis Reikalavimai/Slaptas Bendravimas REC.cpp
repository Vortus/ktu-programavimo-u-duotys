#include <iostream>
#include <iomanip>
#include <fstream>
#include <string.h>

using namespace std;

    int key, messageLength; // sifravimo raktas, zin ilgis
    char encodedMessage[256];

    void readFile(){ // failo nuskaitymas
        ifstream fi("Duomenys1.txt");
        fi >> key; fi.ignore(); // \n praignorina skaitant
        fi.getline(encodedMessage, 256); // paiema eilute
        messageLength = strlen(encodedMessage); // gauna ilgi
        fi.close();
    }

    void push(int sIndex, int k){ // prastiuma masyva nuo sIndex su raktu key
        for(int i = sIndex; i < messageLength; i++)
           for(int j = 0; j < k; j++){
                encodedMessage[i]--;
                if((int)encodedMessage[i] < 97) encodedMessage[i] = 'z';// jei perzengia abeceles riba
            }
    }

    int contains(char arr[], int starts, char c){ // ar yra characteris jei yra returnini indeksa ir padarai tarpa
        for(int i = starts; i < messageLength; i++)
            if(arr[i] == c){ arr[i] = ' '; return i; }
        return -1;
    }

    void decodeMessageRec(int starts){ // atkoduot rekursyviai
        push(starts, key);
        int xIndex = contains(encodedMessage, starts, 'x');
        if(xIndex == -1) return; key = xIndex - starts;
        decodeMessageRec(xIndex + 1);
    }

 int main(){
    readFile();
    decodeMessageRec(0);
    ofstream fo("Rezultatai1.txt");
    fo << "Issifruota zinute: " << endl << encodedMessage;
    fo.close();

 return 0; }

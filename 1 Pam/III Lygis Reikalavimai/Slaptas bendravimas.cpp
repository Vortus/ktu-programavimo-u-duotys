#include <iostream>
#include <iomanip>
#include <fstream>
#include <string.h>

using namespace std;

    int key, messageLength; // sifravimo raktas, zin ilgis
    char encodedMessage[256];

    void readFile(){ // failo nuskaitymas
        ifstream fi("Duomenys1.txt");
        fi >> key; fi.ignore(); // \n praignorina skaitant
        fi.getline(encodedMessage, 256); // paiema eilute
        messageLength = strlen(encodedMessage); // gauna ilgi
        fi.close();
    }

    void push(int sIndex, int k){ // prastiuma masyva nuo sIndex su raktu key
        for(int i = sIndex; i < messageLength; i++)
           for(int j = 0; j < k; j++){
                encodedMessage[i]--;
                if((int)encodedMessage[i] < 97) encodedMessage[i] = 'z';// jei perzengia abeceles riba
            }
    }

    void decodeMessage(){ // zinutes issifravimas
        int l = 0;// einamos zinutes ilgis
        for(int i = 0; i < messageLength; i++){
            if(encodedMessage[i] == 'x'){
                    push(i, l); encodedMessage[i] = ' '; l = 0; // issifruoji nuo i indekso su raktu l , paresetini l
            }
            else l++; // jei ne tai ilgis didej
        }
    }

 int main(){

    readFile();
    push(0, key); // pirmasis prastumimas
    decodeMessage();

    ofstream fo("Rezultatai1.txt");
    fo << "I��ifruota �inut�: " << endl << encodedMessage;
    fo.close();

 return 0; }

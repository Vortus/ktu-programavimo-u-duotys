#include <iostream>
#include <iomanip>
#include <fstream>
#include <string.h>
#include <cstring>

using namespace std;

    struct Shorten{ // strumpinimo struktas
        char name[11]; // sutrumpinimo pav
        char text[256]; // sutrumpinimo tekstas
    } shortens[1000];

    int shortenCount;
    char text[1000], tmpText[1000]; // galutinis tekstas, laikinas;

    void readFile(){ // failo nuskaitymas
        ifstream fi("Duomenys3.txt");
        fi >> shortenCount; // trumpiniu skaicius

        for(int i = 0; i < shortenCount; i++){
            fi >> shortens[i].name; fi.ignore(); // tarpo neimti
            fi.getline(shortens[i].text, 256);
        }

        fi.getline(tmpText, 1000); //nuskaito i laikina is kurio reikes perkelt
        fi.close();
    }

    bool goodShorten(char name[]){ // ar atitinka reikalavimus
        if(strlen(name) > 10) return false;
        for(int i = 0; i < strlen(name); i++)
            if(name[i] < 65 || name[i] > 90) return false;
        return true;
    }

    bool foundShorten(int start, int end, Shorten s){ //ar duotasis masyvo intervalas attitinka sutrumpinima
        if(end - start != strlen(s.name)) return false; // jei intervalu ilgiai skiriasi netikrinti iskart
        int l = 0;
        for(int i = start; i < end; i++){
            if(tmpText[i] != s.name[l]) return false;
            l++;
        }
        return true;
    }

    void newArray(){ // naujas simboliu masyvas ... ==> text
        for(int i = 0; i < strlen(tmpText); i++){
            if(tmpText[i] == '['){
                int l = 0; // ilgis sutrumpinimo
                while(tmpText[i + l + 1] != ']') l++;
                    for(int j = 0; j < shortenCount; j++){
                        if(goodShorten(shortens[j].name) && foundShorten(i + 1, i + l + 1, shortens[j])) {
                            strcat(text, shortens[j].text);
                            i += l + 2; // +2 del skliaustu
                            break;
                        }
                    }
               }
            text[strlen(text)] = tmpText[i];
        }
    }


 int main(){

    readFile();
    newArray();

    ofstream fo("Rezultatai3.txt");
    fo << text;
    fo.close();

 return 0; }

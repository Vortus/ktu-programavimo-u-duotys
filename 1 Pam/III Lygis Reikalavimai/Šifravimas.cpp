#include <iostream>
#include <iomanip>
#include <fstream>
#include <map>
#include <string.h>

using namespace std;

    struct Message{
        int day;
        char sender[256];
        char text[1000];
        bool spaceCheck[1000]; // cia del tarpo nes gali dubliuotis orginalus ir uzkoduotas tarpai
    } allMessages[1000];

    map<char, char> encoder, decoder; // sifravimo atsifravimo
    int nom; // kiek isviso zinuciu

    int fact(int n){
        if(n <= 1) return 1;
        return fact(n - 1) * n;
    }

    void readEncryptFile(){ // sifro failo nuskaitymas
        ifstream fi("Sifras.txt");
        while(!fi.eof()){ // kol pabaiga
            char a, b; fi.get(a); fi.ignore(); fi.get(b); fi.ignore(); // nuskatio simboli ir praleidzia tarpa
            encoder[a] = b;
            decoder[b] = a;
        }
        fi.close();
    }

    void readMessageFile(){ // zinuciu failas
        ifstream fi("Zinute.txt");
        fi >> nom;
        for(int i = 0; i < nom; i++){
            fi >> allMessages[i].day; fi.ignore(); // dienos
            fi.getline(allMessages[i].sender, 256);// vardas
            fi.getline(allMessages[i].text, 1000);// zinute
        }
        fi.close();
    }

    void encrypt(char message[], int index){ // zinutes tekstas, indeksas zinutes
        int f = fact(allMessages[index].day); // faktorialas
        for(int i = 0; i < strlen(message); i++){ // per visus simbolius
            if(message[i] == ' '){ allMessages[index].spaceCheck[i] = true; i++; } // jeigu normalus tarpas
            message[i] = encoder[message[i]]; // sukeiti raidem
            if(allMessages[index].day != 1) message[i] += f; // jei nepirmadienis prastumi
        }
    }

    void decrypt(char message[], int index){ // zinutes tekstas, indeksas zinutes
        int f = fact(allMessages[index].day); // faktorialas
        for(int i = 0; i < strlen(message); i++){ // per visus simbolius
            if(allMessages[index].spaceCheck[i] && message[i] == ' '){ message[i] = ' '; i++; }// jei normalus tarpas
            if(allMessages[index].day != 1) message[i] -= f; // jei nepirmadienis prastumi
            message[i] = decoder[message[i]]; // sukeiti raidem
        }
    }

    string dayToStr(int i){ // dienos skaicius i stringa
        if(i == 1) return "Pirmadienis";
        if(i == 2) return "Antradienis";
        if(i == 3) return "Treciadienis";
        if(i == 4) return "Ketvirtadienis";
        if(i == 5) return "Penktadienis";
        if(i == 6) return "Sestadienis";
        return "Sekmadienis";
    }

    string returnLine(){ // linija
        return "-----------------------------------------------------------------------------------------------------";
    }

    void returnToFile(){ // isvedimas i faila
        ofstream fo("Rezultatai1.txt");
        for(int i = 0; i < nom; i++){
            fo << returnLine() << endl;
            fo << "Siuntejas: " << allMessages[i].sender << endl;
            fo << "Gauta: " << dayToStr(allMessages[i].day) << endl;
            encrypt(allMessages[i].text, i);
            fo << "Uzsifruota zinute: " << allMessages[i].text << endl;
            decrypt(allMessages[i].text, i);
            fo << "Issifruota zinute: " << allMessages[i].text << endl;
            fo << returnLine() << endl;
        }
        fo << returnLine() << endl;
        fo.close();
    }

 int main(){

    readEncryptFile();
    readMessageFile();
    returnToFile();

 return 0; }

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

   int pilgis, dilgis;
   string email; // stringas ir yra simboliu masyvas

    void readFile(string s){ // failo nuskaitymas
        ifstream fi(s);
        fi >> pilgis >> dilgis >> email; // pasto ilgis domeno ilgis ir pats emailas
        fi.close();
    }

    bool goodCharacters(string s){ // ar geri simboliai
        for(int i = 0; i < s.length(); i++){
            int charn = (int)s[i];
            if((charn >= 65 && charn <= 90) // nuo A-Z
               || (charn >= 97 && charn <= 122) // nuo a-z
               || charn == 46){ } // tasko simbolis
               else return false; // jeigu netinka simboliai
        }
        return true;
    }

    int check(){ // tikrinimo funkcija
        int etaIndex = email.find("@"); // etos indeksas
        if(etaIndex > 400){ cout << "Email neteisingas " << endl << "N�ra @ �enklo."; return 0; } // jei nera etos
        string first = email.substr(0, etaIndex); // pasiemu pradzia
        if(!goodCharacters(first)) { cout << "Email neteisingas " << endl << "Yra neleid�iam� simboli�."; return 0; } // ar yra neleistinu simboliu
        string last = email.substr(etaIndex + 1); // pasiemu gala
        int domaindotIndex = last.find("."); // galunes tasko indeksas
        if(domaindotIndex > 400) { cout << "Email neteisingas " << endl << "Neteisingas domenas."; return 0; }
        string pdez = last.substr(0, domaindotIndex); // gaunu pasto dezute
        string pdom = last.substr(domaindotIndex + 1); // gaunu domena
        if(pdez.length() < pilgis) { cout << "Email neteisingas " << endl << "Per trumpas pa�to d��ut�s pavadinimas."; return 0; } // pasto dez per trumpa
        if(pdom.length() < dilgis) { cout << "Email neteisingas " << endl << "Per trumpas domeno pavadinimas."; return 0; } // domenas per trumpas
        if(!goodCharacters(pdez)) { cout << "Email neteisingas " << endl << "Yra neleid�iam� simboli�."; return 0; }// ar yra neleistinu simboliu
        cout << "Email teisingas."; // jei viskas okey
        return 1;
    }

 int main(){

    setlocale(LC_ALL, "Lithuanian"); // kad lt raides matitus
    readFile("email.txt");
    check();

 return 0; }

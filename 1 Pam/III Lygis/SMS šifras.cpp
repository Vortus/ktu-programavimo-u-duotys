#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <map>

using namespace std;

    map<string, char> keyboard;

    string sequence; // seka
    void setKeyboard(); // sudeda klaviatura

     void readFile(string s){ // nuskaito faila
        ifstream fi(s);
        int n; fi >> n; // nereikalingas visiskai
        char c;
        while(c != '1'){
            fi.get(c);
            if(c != '\n') sequence += c;
        }
        fi.close();
    }

    string translate(string s){ // isvercia
        int i = 0; string tmp, result;
        while(i < s.length()){
            if(s[i] == '0'){
                result += keyboard[tmp];
                tmp = "";
            }else tmp += s[i];
            i++;
        }

        result += keyboard[tmp.substr(0, tmp.length() - 1)]; // kadangi atlieka dar vienas veiksmas
        return result + ".";
    }

 int main(){

    setKeyboard();
    readFile("Duomenys4.txt");

    ofstream fo("Rezultatai4.txt");
    fo << translate(sequence);
    fo.close();

 return 0; }

    void setKeyboard(){ // klaviaturos nustatymai

        keyboard["2"] = 'a';
        keyboard["22"] = 'b';
        keyboard["222"] = 'c';
        keyboard["3"] = 'd';
        keyboard["33"] = 'e';
        keyboard["333"] = 'f';
        keyboard["4"] = 'g';
        keyboard["44"] = 'h';
        keyboard["444"] = 'i';
        keyboard["5"] = 'j';
        keyboard["55"] = 'k';
        keyboard["555"] = 'l';
        keyboard["6"] = 'm';
        keyboard["66"] = 'n';
        keyboard["666"] = 'o';
        keyboard["7"] = 'p';
        keyboard["77"] = 'q';
        keyboard["777"] = 'r';
        keyboard["7777"] = 's';
        keyboard["8"] = 't';
        keyboard["88"] = 'u';
        keyboard["888"] = 'v';
        keyboard["9"] = 'w';
        keyboard["99"] = 'x';
        keyboard["999"] = 'y';
        keyboard["9999"] = 'z';
        keyboard["00"] = ' ';
    }

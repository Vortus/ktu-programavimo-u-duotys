#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int maxSymbols; // maksimalus simboliu kiekis naudojant funkcija find

    string moveLeft(string s, int times){ // simboliu masyvo perstumimas i kaire
        for(int i = 0; i < s.length(); i++)
            for(int j = 0; j < times; j++){
                s[i]--;
                if((int)s[i] < 97) s[i] = 'z';// jei perzengia abeceles riba
            }
        return s;
    }

    string decodeRec(string s, int r){ // recursyviai
        s = moveLeft(s, r); // prastumia i sona
        if(s.find("x") > maxSymbols) return s;// iesko ar x yra
        string d = s.substr(0, s.find("x")); // paema isversta dali
        return d + " " + decodeRec(s.substr(s.find("x") + 1), d.length());
    }

 int main(){

    int r; string s; // simboliu masyvas
    ifstream fi("Duomenys1.txt");
    fi >> r >> s; maxSymbols = s.length(); // failo nuskaitymas
    fi.close();
    ofstream fo("Rezultatai1.txt");
    fo << decodeRec(s, r); // isspausdina rezultata
    fo.close();

 return 0; }

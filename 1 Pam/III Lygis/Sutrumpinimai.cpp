#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    struct Shorten{ // sutrumpinimo struktura
        string name, text;
        bool g = false;
    };

    bool goodShorten(string s){ // ar geras sutrumpinimas
        if(s.length() > 10) return false;
        for(int i = 0; i < s.length(); i++)
            if(s[i] < 65 && s[i] > 90) return false;
        return true;
    }

    string returnInserted(Shorten arr[], string s, int n){ // grazina sudeta simboliu masyva
        for(int i = 0; i < n; i++){
            if(arr[i].g){ // jei galima
                int index = s.find(arr[i].name);
                s = s.substr(0, index - 1) + arr[i].text +
                s.substr(index + arr[i].name.length() + 1);
            }
        }
        return s;
    }

 int main(){

    ////////////Failo nuskaitymas
    ifstream fi("Duomenys3.txt");
    int n, available = 0; fi >> n; // trumpiniu kiekis
    Shorten trumpiniai[n];

    for(int i = 0; i < n; i++){
        string tr, tx;
        fi >> tr; fi.ignore(); getline(fi, tx);
        if(goodShorten(tr)){
            trumpiniai[i].name = tr;
            trumpiniai[i].text = tx;
            trumpiniai[i].g = true;
        }
    }
    string tekstas; getline(fi, tekstas);
    fi.close();
    ////////////

    tekstas = returnInserted(trumpiniai, tekstas, n);
    ofstream fo("Rezultatai3.txt");
    fo << tekstas;
    fo.close();

 return 0; }

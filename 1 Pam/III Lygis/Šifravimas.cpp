#include <iostream>
#include <iomanip>
#include <fstream>
#include <map>

using namespace std;

    struct Message{ // zinutes structas
        int day;
        string sender, text;
    } allMessages[10];

    map<char,char> encryptor; // as manau mapas labiau tinka
    map<char,char> reverseEncryptor; // tik gal maziau efektyvu kadangi reikia dvieju

    int n; // zinuciu kiekis

    int fact(int n, int value){
        if(n <= 1) return value;
        return fact(n - 1, value * n);
    }

   void readMessageFile(string s){ // zinutes failo nuskaitymas
        ifstream fi(s); fi >> n;
        for(int i = 0; i < n; i++){
            int j; fi >> j; // diena
            string tmps; fi >> tmps; fi.ignore(); // siuntejas
            string text; getline(fi, text); // tekstas
            allMessages[i].day = j;
            allMessages[i].sender = tmps;
            allMessages[i].text = text;
        }
        fi.close();
    }

   void readEncryptionFile(string s){ // uzsifravimo nuskaitymas
        ifstream fi(s);
        for(int i = 0; i < 25; i++){
            //nuskaito kas antra simboli kadangi mum tarpo reikia
            char a, b; fi.get(a); fi.ignore(); fi.get(b); fi.ignore();
            encryptor[a] = b;
            reverseEncryptor[b] = a;
        }
        fi.close();
    }

    void encryptMessage(string &s, int day){//uzsifravimas
        int f = fact(day, 1); // dienos faktorialas
        for(int i = 0; i < s.length(); i++){
            s[i] = encryptor[s[i]]; // uzsifruota raide
            if(day != 1){ s[i] += f; } // jei nepirmadienis prastumt
        }
    }

    void decryptMessage(string &s, int day){//atsifravimas
        int f = fact(day, 1); // dienos faktorialas
        for(int i = 0; i < s.length(); i++){
            if(day != 1){ s[i] -= f; } // jei nepirmadienis prastumt
            s[i] = reverseEncryptor[s[i]]; // uzsifruota raide
        }
    }

    string dayToStr(int i){ // dienos skaicius i stringa
        if(i == 1) return "Pirmadienis";
        if(i == 2) return "Antradienis";
        if(i == 3) return "Tre�iadienis";
        if(i == 4) return "Ketvirtadienis";
        if(i == 5) return "Penktadienis";
        if(i == 6) return "�e�tadienis";
        return "Sekmadienis";
    }

    string returnLine(){ // linija
        return "-----------------------------------------------------------------------------------------------------";
    }

    void returnToFile(string s){ // isvedimas i faila
        ofstream fo(s);
        for(int i = 0; i < n; i++){
            fo << returnLine() << endl;
            fo << "Siuntejas: " << allMessages[i].sender << endl;
            fo << "Gauta: " << dayToStr(allMessages[i].day) << endl;
            encryptMessage(allMessages[i].text, allMessages[i].day);
            fo << "�ifruota �inut�: " << allMessages[i].text << endl;
            decryptMessage(allMessages[i].text, allMessages[i].day);
            fo << "I��ifruota �inut�: " << allMessages[i].text << endl;
            fo << returnLine() << endl;
        }
        fo << returnLine() << endl;
        fo.close();
    }

 int main(){
    char c[10];
    string s = ",";
    c[0] = ',';
    cout << (char)130 <<  " " << (int) c[0];

    readMessageFile("Zinute.txt");
    readEncryptionFile("Sifras.txt");
    returnToFile("Rezultatai1.txt");

 return 0; }

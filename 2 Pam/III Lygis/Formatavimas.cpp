#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

    vector<string> lines;
    int bracketC = 0;
    string tmpText;

    void removeFrontSpaces(string &line){ // isema priekinius spaces
        for(int i = 0; i < line.length(); i++)
            if(line[i] != ' '){ line.erase(0, i); return; }
    }

    void readFile(){ // skaitofaila ir tuo paciu metu bando tvarkyti bracketus
        ifstream fi("Duom.txt");
        while(!fi.eof()){
            string line; getline(fi, line);
           // removeFrontSpaces(line);
            int cb = line.find("{");
            if(cb != -1){ line.insert(cb, "\n"); line.insert(cb + 2, "\n"); }
            int cb2 = line.find("}");
            if(cb2 != -1){ line.insert(cb2, "\n"); line.insert(cb2 + 2, "\n"); }
            line += "\n";
            tmpText += line;
        }
        fi.close();
    }

    void removeNewLineDuplicates(){ // isema duplikuotus new linus, galutinai sutvarko braketus

        for(int i = 0; i < tmpText.length() - 1; i++){
            if(tmpText[i] == '\n' && tmpText[i + 1] == '\n') { tmpText.erase(i, 1);  }
        }
    }

    void splitByNewLine(){ // suskirsto i vectoriu
        string tmp;
        for(int i = 0; i < tmpText.length(); i++){
            if(tmpText[i] == '\n'){
                lines.push_back(tmp);
                tmp = "";
                i++;
            }
            tmp += tmpText[i];
        }
    }

    void insertSpacesToFront(string &line, int count){ // imeta i prieki kiek reikai tarpu
        int c = count;
        for(int i = 0; i < line.length(); i++)
            if(line[i] == ' ') c--;
            else break;
        string sp;
        for(int i = 0; i < c; i++)
            sp += " ";
        line.insert(0, sp);
    }

    bool scrap(int index){ // ar yra nereikalingas
        bool b = true;
        for(int i = 0; i < lines.at(index).length(); i++)
            if(lines.at(index)[i] != ' ') return false;
        return b;
    }

    void formatSpaces(){ // suformatuoja tarpus ir iseman ereikalingas atliekas
        for(int i = 0; i < lines.size(); i++){
            if(scrap(i)) lines.erase (lines.begin()+i);
            if(lines.at(i).find("}") != -1) bracketC--;
            insertSpacesToFront(lines.at(i), bracketC * 4);
            if(lines.at(i).find("{") != -1) bracketC++;
        }
    }

 int main(){

    setlocale(LC_ALL, "Lithuanian");
    readFile();

    removeNewLineDuplicates();
    splitByNewLine();
    formatSpaces();

    ofstream fo("Rezultatai.txt");
    for(int i = 0; i < lines.size(); i++)
        fo << lines.at(i) << endl;
    fo.close();


 return 0; }

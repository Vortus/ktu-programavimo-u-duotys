#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    /*
        Sprendimas: Recursyvas su backtrakingu
        Galima labiau optimizuotai padaryt bet tingejau.
    */

    int bSize, wordCount; // boardo dydis , zodziu kiekis
    char board[1000][1000]; // y , x boardas
    char resultBoard[1000][1000]; // rezultatu lenta
    string words[1000]; // zodziu masyvas

    void readFile(){
        ifstream fi("Kryziazodis.txt");
        fi >> bSize;

        for(int i = 0; i < bSize; i++)
            for(int j = 0; j < bSize; j++){
                fi >> board[i][j];
                resultBoard[i][j] = '+';
            }

        fi.close();
        fi.open("Zodziai.txt");
        fi >> wordCount;
        for(int i = 0; i < wordCount; i++)
            fi >> words[i];
        fi.close();
    }

    //last move: 1 horiz desine 2 horiz kaire
    //3 vertik virsu, 4 vertikaliai apcae
    // 5 6 7 8: NW, SE, NE, SW
    bool explore(int x, int y, string word, int index, int lastMove){
        if(board[y][x] != word[index])
            return false;

        if(word.length() - 1 == index){ // jei rado
            resultBoard[y][x] = word[index];
            cout << word << " " << index << endl;
            return true;
        }

        if((lastMove == 0 || lastMove == 2) && x != 0 && explore(x - 1, y, word, index + 1, 2)){ resultBoard[y][x] = word[index]; return true; } // kaire
        if((lastMove == 0 || lastMove == 1) && x != bSize - 1 && explore(x + 1, y, word, index + 1, 1)){ resultBoard[y][x] = word[index]; return true; } // desine

        if((lastMove == 0 || lastMove == 3) && y != 0 && explore(x, y - 1, word, index + 1, 3)){ resultBoard[y][x] = word[index]; return true; } // virsus
        if((lastMove == 0 || lastMove == 4) && y != bSize - 1 && explore(x, y + 1, word, index + 1, 4)){ resultBoard[y][x] = word[index]; return true; } // apacia

        if((lastMove == 0 || lastMove == 5) && x != 0 && y != 0 && explore(x - 1, y - 1, word, index + 1, 5)){ resultBoard[y][x] = word[index]; return true; } // siaures vakarai
        if((lastMove == 0 || lastMove == 6) && x != bSize - 1 && y != bSize - 1 && explore(x + 1, y + 1, word, index + 1, 6)){ resultBoard[y][x] = word[index]; return true; } // pietu rytai
        if((lastMove == 0 || lastMove == 7) && x != bSize - 1 && y != 0 && explore(x + 1, y - 1, word, index + 1, 7)){ resultBoard[y][x] = word[index]; return true; } // siaures rytai
        if((lastMove == 0 || lastMove == 8) && x != 0 && y != bSize - 1 && explore(x - 1, y + 1, word, index + 1, 8)){ resultBoard[y][x] = word[index]; return true; } // pietvakariai

        return false;
    }

 int main(){

    readFile();

    for(int i = 0; i < bSize; i++){ // iseksplorina
        for(int j = 0; j < bSize; j++){
            for(int k = 0; k < wordCount; k++)
                     explore(j, i, words[k], 0, 0);
        }
    }

    ofstream fo("Rezultatas.txt");
    for(int i = 0; i < bSize; i++){
        for(int j = 0; j < bSize; j++){
            fo << resultBoard[i][j];
        }
        fo << endl;
    }
    fo.close();

 return 0; }

#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    struct Player{ // zaidejo struktas
        bool explored;
        string name;
        char card;
        int x, y;
    } players[11];

    int noPlayers, xp = 0; // zaideju kiekis
    string word, path; // zodis kelias

    void readFile(){

        ifstream fi("Zaidejai.txt");
        fi >> noPlayers;
        for(int i = 0; i < noPlayers; i++){
            string n; char c; int a, b;
            fi >> n >> c >> a >> b;
            players[i].name = n;
            players[i].card = c;
            players[i].x = a;
            players[i].y = b;
        }
        fi.close();
    }

    bool explore(Player &p){
        if(xp == noPlayers + 1){ // jei rado kelia
            path = p.name + "\n" + path;
            word += p.card;
            return true;
        }
        p.explored = true;
        xp++;

        if(!players[p.x - 1].explored && explore(players[p.x - 1])){ path = p.name + "\n" + path; word = p.card + word; return true;}
        if(!players[p.y - 1].explored && explore(players[p.y - 1])){ path = p.name + "\n" + path; word = p.card + word; return true;}

        // jei neina nei vienur gryzti atgal stepu ir tikrini toliau
        return false;
    }

 int main(){

	readFile();

	int min = 11, index = 0; // susiranda pirma zaideja
	for(int i = 0; i < noPlayers; i++){
        if(players[i].y == 0 && players[i].x < min){
            min = players[i].x;
            index = i;
        }
	}

	Player starter = players[index];
    explore(starter);
    ofstream fo("Rezultatai.txt");
    fo << path;
    fo << "Zodis: " << word;
    fo.close();

 return 0; }

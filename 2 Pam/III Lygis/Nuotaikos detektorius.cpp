#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

using namespace std;

    struct Data{ // struktas
        string words[1000];
        int wordCount;
    } data;

    void readFile(string s){ // nuskaitymas
        ifstream fi(s);
        fi >> data.wordCount;
        for(int i = 0; i < data.wordCount; i++)
            fi >> data.words[i];
        fi.close();
    }

    void toFile(string s, string result){ // irasimas
        ofstream fo(s);
        fo << result;
        fo.close();
    }

    bool isPalindrome(string s){ // ar zodis palindroams
        int i = 0, j = s.length() - 1; // du cursoriai
        while(i < j){
            if(s[i] != s[j] && i < j)
                return false;
            i++; j--;
        }
        return true;
    }

    bool isExciting(string s){ // pakilus
        for(int i = 0; i < s.length() - 1; i++){
            int a = (int)s[i], b = (int) s[i + 1];
            if(a <= 0) a += 256; if(b <= 0) b+= 256;
            if(a > b) return false;
        }
        return true;
    }

    void getResults(){ // rezultatai
        int palindr = 0, exc = 0;
        for(int i = 0; i < data.wordCount; i++){
            if(isExciting(data.words[i])) exc++;
            if(isPalindrome(data.words[i])) palindr++;
        }
        if(palindr == exc) toFile("Rezultatai.txt", ":|");
        else if(palindr < exc) toFile("Rezultatai.txt", ":)");
        else toFile("Rezultatai.txt", ":(");
    }

 int main(){

    readFile("Duomenys.txt");
    getResults();

 return 0; }

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    /*
    Para�ykite funkcij�, kuri nustatyt� kiek kart� nurodytas �odis kartojasi tekste.
    Sita funkcija yra bereikalingas darbas programai jau pats tikrinimas ar yra zodis masyve yra gan sunkus.
    O jei turime koki 1000 zodziu sakini tai visiskai bereikalingai.
    */

    struct Word{  // zodzio struktas
        string text;
        int times = 1;
    } words[1000];

    int allWords = 0;

    string message; // visa zinute
    string seperators = " .,;:�!?()"; // skyriliai

    void readFile(string s){ // failo nuskaitymas
        ifstream fi(s);
        getline(fi, message);
        fi.close();
    }

    bool isSeperator(char c){
        for(int i = 0; i < seperators.length(); i++)
            if(c == seperators[i]) return true;
        return false;
    }

    int exists(string s, int n){ // ar yra masyve
        for(int i = 0; i < n; i++)
            if(words[i].text == s) return i;
        return -1;
    }

    void skip(int &i){ // praleidzia visus seperatorius
        while(isSeperator(message[i + 1]))
            i++;
    }

    void splitter(){ // sudalinti zodzius
        string tmp; int tmpW = 0;
        for(int i = 0; i < message.length(); i++){
            if(isSeperator(message[i])){
                int ex = exists(tmp, tmpW); // vietoj fukncijos kiek kartu masyve tuo paciu metu gauni indeksa
                if(ex == -1){
                    words[tmpW].text = tmp;
                    tmp = "";
                    tmpW++;
                }else{ // ir jei randa pasikartojima tai prideda prie kartu
                    words[ex].times++;
                }
                skip(i);
                tmp = "";
            }else tmp += message[i];
        }
        allWords = tmpW;
    }

 int main(){

    setlocale(LC_ALL, "Lithuanian");
    readFile("Duomenys.txt");
    splitter();

    ofstream fo("Rezultatai.txt");
    fo << "I� viso skirting� �od�i� " << allWords << endl;

    for(int i = 0; i < allWords; i++)
        fo << i + 1 << ". " << words[i].text << " " << words[i].times << endl;

    fo.close();

 return 0; }

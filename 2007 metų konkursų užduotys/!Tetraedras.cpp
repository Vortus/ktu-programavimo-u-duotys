#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>

using namespace std;


    struct Triangle{
        int a, b, c;
        void show(){
            cout << a << " " << b << " " << c << endl;
        }

    }arr[1000];

    int n;
    vector<Triangle> availableTriangles; // galimi lygiakrasciai trikampiai

    bool sameSide(int a, int b, int c){
        return a == b || b == c || a == c ? true : false;
    }

    void readFile(string s){
        ifstream fi(s);
        fi >> n;
        for(int i = 0; i < n; i++){
            int a, b, c; fi >> a >> b >> c;
            arr[i].a = a;
            arr[i].b = b;
            arr[i].c = c;
            if(sameSide(a, b, c)) availableTriangles.push_back(arr[i]);
        }
        fi.close();
    }

 int main(){

    readFile("KU2.txt");


 return 0; }

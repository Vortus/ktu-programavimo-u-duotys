#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int n;

    string decToBin(int n){
        string res;
        char c;
        while(n > 0){
            int j = n % 2;
            c = j + '0';
            res = c + res;
            n /= 2;
        }
        return res;
    }

    void readAndOutFile(string s){
        ifstream fi(s);
        int k; fi >> k;
        cout << k + 1 << endl;
        cout << decToBin(k + 1);
        fi.close();
    }

 int main(){

    readAndOutFile("SU1.txt");

 return 0; }

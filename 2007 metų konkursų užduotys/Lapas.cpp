#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int board[1000][1000];
    int w, h;

    void readFile(string s){
        ifstream fi(s);
        fi >> h >> w;
        for(int i = 0; i < h; i++)
            for(int j = 0; j < w; j++)
                fi >> board[i][j];
        fi.close();
    }

    bool explore(int x, int y){
        if(board[y][x] == 0 || board[y][x] == 2) return false; // jei iskerpa arba buvom
        board[y][x] = 2; // explored
        if(x != 0) explore(x - 1, y);
        if(y != 0) explore(x, y - 1);
        if(x + 1 != w) explore(x + 1, y);
        if(y + 1 != h) explore(x, y + 1);
        return true;
    }


 int main(){

    readFile("KU3.txt");
    int c = 0;

   for(int i = 0; i < h; i++){
        for(int j = 0; j < w; j++)
            cout << board[i][j];
        cout << endl;
   }

    for(int i = 0; i < h; i++)
        for(int j = 0; j < w; j++){
            bool b = explore(j, i);
            if(b) c++;
        }

    cout << "Gabaliukai " << c;

 return 0; }

#include <iostream>
#include <iomanip>
#include <fstream>
#define FILE "DU2.txt"
using namespace std;

/*
    Brute force approachas, kadangi nera dideli masyvai butu galima naudoti ir DP.
*/

    int N, j;
    int array[110];

    void readFile(){
        ifstream FI(FILE); FI >> N;
        int i = 1; while(FI >> j) array[i++] = j;
        FI.close();
    }

    bool ascending(int a, int b){
        return a < b ? true : false;
    }

    bool goodSubArr(int x, int y){
        if(array[x] == array[y]) return false;
        bool asc = ascending(array[x], array[x + 1]);
        bool result = true;
        if(asc == ascending(array[y - 1], array[y]))
            return false;
        for(int i = x + 1; i <= y - 1; i++){
            bool b = ascending(array[i], array[i + 1]);
            if(b == asc){ result = false; break;}
            else asc = b;
        }
        return result;
    }

 int main(){

    readFile(); int bestI, bestJ, bestL = -1;
    int kk = 0;
    bool found = false;
    for(int i = 1; i <= N - 2; i++){
        for(int j = i + 2; j <= N; j++){
            if(bestL < j - i){
                if(goodSubArr(i, j)){
                    bestI = i; bestJ = j; bestL = j - i;
                    found = true;
                }
            }
        }
    }

    if(found){
        cout << bestI << " " << bestJ << endl;
        for(int i = bestI; i <= bestJ; i++)
            cout << array[i] << " ";
    } else {
        cout << "Nera";
    }

 return 0; }

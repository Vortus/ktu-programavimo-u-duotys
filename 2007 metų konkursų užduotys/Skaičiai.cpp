#include <iostream>
#include <iomanip>
#include <fstream>
#include <string.h>

using namespace std;

    void check(string a, string b){
        int an = stoi(a), bn = stoi(b);
        if(an == bn) cout << an << " = " << bn << endl;
        else if(an > bn) cout << an << " > " << bn << endl;
        else cout << an << " < " << bn << endl;
    }


    void readFile(string s){
        ifstream fi(s);
        int n; fi >> n; fi.ignore();
        char c;

        for(int i = 0; i < 3; i++){
            char al, bl;
            fi.get(al); fi.get(bl);
            string a, b;
            for(int i = 0; i < (al - '0'); i++){
                fi >> c;
                a = c + a;
            }
            fi.ignore();
            for(int i = 0; i < (bl - '0'); i++){
                fi >> c;
                b = c + b;
            }
            check(a, b);
            fi.ignore();
        }
        fi.close();
    }


 int main(){

    readFile("KU1.txt");

 return 0; }

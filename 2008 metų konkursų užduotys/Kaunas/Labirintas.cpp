#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
using namespace std;

    #define x first
    #define y second
    #define FILE "U3.txt"
    #define MAXDIMS 55
    #define MAXD 4

    typedef pair<int, int> Point;
    const Point DIR[] = { Point(0, -1), Point(1, 0), Point(0, 1), Point(-1, 0) };

    int WIDTH, HEIGHT, WALLSIZE = 2;
    char board[MAXDIMS][MAXDIMS];

    void readFile(){
        ifstream FI(FILE); FI >> HEIGHT >> WIDTH;
        for(int i = 0; i < HEIGHT; i++)
            for(int j = 0; j < WIDTH; j++)
                FI >> board[i][j];
        FI.close();
    }

    inline bool validCoordinates(int x, int y){
        return x >= 0 && x < WIDTH && y >= 0 && y < HEIGHT;
    }

 int main(){

    readFile();

    for(int i = 0; i < HEIGHT; i++){
        for(int j = 0; j < WIDTH; j++){
            cout << setw(2) << board[i][j];
        }
        cout << endl;
    }

 return 0; }

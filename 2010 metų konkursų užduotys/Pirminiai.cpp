#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

    int reverse(int n){
        int result = 0;
        while(n > 0){
            int j = n % 10;
            result = 10 * result + j;
            n /= 10;
        }
        return result;
    }

    bool prime(int n){
        if(n <= 1) return false;
        for(int i = 2; i <= sqrt(n); i++)
            if(n % i == 0) return false;
        return true;
    }

 int main(){
    ifstream fi("ZU1.txt");
    int a, b; fi >> a >> b;
    bool d = false;
    for(int i = a; i <= b; i++)
    if(prime(i)){
        if(!d) d = true;
        cout << i << " ";
        if(prime(reverse(i))) cout << "TAIP" << endl;
        else cout << "NE" << endl;
    }
    if(!d) cout << "NERA";
    fi.close();

 return 0; }

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include <vector>

using namespace std;

    bool sum(int n, int s){
        int result = 0;
        vector<int> tmp;
        while(n > 0){
            int j = n % 10;
            for(int i = 0; i < tmp.size(); i++)
                if(j == tmp.at(i)) return false;
            tmp.push_back(j);
            result += j;
            n /= 10;
        }

        if(result == s) return true;
        return false;
    }

    void get(string s){
        ifstream fi(s);
        int n; fi >> n;
        for(int i = 0; i < n; i++){
            int mins = -1, maxs = -1;
            int a, b; fi >> a >> b;
            for(int i = pow(10, a - 1); i <= pow(10, a) - 1; i++)
                 if(sum(i, b)){ mins = i; break; }
            for(int i = pow(10, a) - 1; i >= pow(10, a - 1); i--)
                 if(sum(i, b)){ maxs = i; break; }
            if(mins > -1 && maxs > -1) cout << mins << " " << maxs << endl;
            else cout << "Nera";
        }

        fi.close();
    }

 int main(){

    get("KU1.txt");

 return 0; }

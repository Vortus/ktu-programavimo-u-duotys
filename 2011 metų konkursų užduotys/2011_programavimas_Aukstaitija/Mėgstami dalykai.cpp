#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
using namespace std;

    #define FILE "U2.txt"
    #define MAXN 11

    struct Lesson { // pamokos irasas
        string name;
        int ID, count;
        void initialize(string n, int nr){ name = n; ID = nr; }
    } lessons[MAXN];

    int N; // pamoku kiekis

    bool sortByCount(Lesson a, Lesson b){
        return a.count > b.count;
    }

    Lesson *findLessonByID(int id){ // randa pamoka pagal id
        int i;
        for(i = 0; i < N; i++)
            if(lessons[i].ID == id) return &lessons[i];
    }

    void processLessons(ifstream &FI){ // pamokos
        FI >> N;
        int i, a; char name[16];
        for(i = 0; i < N; i++){
            FI.ignore();
            FI.get(name, sizeof name); FI >> a;
            lessons[i].initialize(name, a);
        }
    }

    void processStudents(ifstream &FI){ // mokiniai
        int NN, i, j, k, m; FI >> NN;
        for(i = 0; i < NN; i++){
            FI.ignore(11); FI >> k;
            for(j = 0; j < k; j++){
                FI >> m;
                findLessonByID(m) -> count++;
            }
        }
    }

    void displayResults(){
        int i;
        for(i = 0; i < N; i++){
            if(lessons[i].count <= 0) break;
            cout << lessons[i].name << endl;
        }
    }

 int main(){

    //failo nuskatymas
    ifstream FI(FILE);
    processLessons(FI);
    processStudents(FI);
    FI.close();

    //surikiavimas
    sort(lessons, lessons + N, sortByCount);

    //isvedimas
    displayResults();

 return 0; }

#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
using namespace std;

    #define FILE "U1.txt"

    int months[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }; // menesiai

    bool isLeapYear(int n){ // ar keliamieji metai
        return (n % 400 == 0) || (n % 100 != 0 && n % 4 == 0);
    }

    int N; // norimu menesiu kiekis

    void findIMonths(int year, int fday){ // metai, pirma diena
        int leapAddup = isLeapYear(year), fridays, sundays, saturdays, m, d;
        cout << year << ": ";
        for(m = 1; m <= 12; m++){ // per visus menesius
            fridays = 0, sundays = 0, saturdays = 0;
            for(d = 1; d <= (m == 2 ? months[m] + leapAddup : months[m]); d++){ // einam per dienas
                if(fday == 5) fridays++;
                if(fday == 6) sundays++;
                if(fday == 7) saturdays++;
                fday = (fday % 7) + 1; // pereinam prie kitos savaites dienos
            }
            if(fridays == 5 && sundays == 5 && saturdays == 5)
                cout << m << " ";
        }
        cout << endl;
    }

    int findFirstDay(int year){
        int y, m, d, resultDay = 1, leapAddup; // Reik�m�s ir resultat� diena
        for(y = 1990; y < year; y++){ // Per visus metus, nuo 1990 iki (ne�skaitant) ie�kom�
            leapAddup = isLeapYear(y);
            for(m = 1; m <= 12; m++){ // Per visus m�nesius
                for(d = 1; d <= (m == 2 ? months[m] + leapAddup : months[m]); d++){ // Per visas dienas nepamir�tam keliam�ju
                    resultDay = (resultDay % 7) + 1; // Pereinam prie kitos savait�s dienos
                }
            }
        }
        return resultDay;
    }

    int rotateInt(int n){
        int result = 0;
        do{
            result = result * 10 + (n % 10);
        } while((n = n / 10) > 0);
        return result;
    }

 int main(){

    ifstream FI(FILE); FI >> N;
//    cout << findFirstDay(2003);
    int tmpa, tmpb, i;
//    for(i = 0; i < N; i++){
//        FI >> tmpa >> tmpb;
//        findIMonths(tmpa, tmpb);
//    }
    FI.close();

    cout << rotateInt(123456789);

 return 0; }

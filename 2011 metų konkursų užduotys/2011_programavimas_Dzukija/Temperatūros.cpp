#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int n; // kiekis
    double board[51][7];

    void readFile(string s){
        ifstream fi(s);
        fi >> n;
        for(int i = 0; i < n; i++)
            for(int j = 0; j < 7; j++)
                fi >> board[i][j];
        fi.close();
    }

    double maxAverage(){
        double result = (board[0][0] + board[0][1] + board[0][2]) / 3;
        for(int i = 0; i < n; i++){
            for(int j = 0; j < 5; j++){
                double sum = (board[i][j] + board[i][j + 1] + board[i][j + 2]);
                if(sum > result) result = sum;
            }
        }
        return result;
    }


 int main(){

    readFile("U1.txt");
    double maxs = maxAverage();

    for(int i = 0; i < n; i++){
        for(int j = 0; j < 5; j++){
            double sum = (board[i][j] + board[i][j + 1] + board[i][j + 2]);
            if(sum == maxs) cout << "Nr: " << i + 1 << " -> " <<
                board[i][j] << " "
                 << board[i][j + 1] << " " << board[i][j + 2] << endl;
        }
    }

 return 0; }

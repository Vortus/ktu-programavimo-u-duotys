#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    char pieceSet[1001]; // setas
    bool pieceSetExplored[1001];
    int pieceCounts[6];
    int pieceCount; // kiekis

    void refreshPieceCounts(){
        for(int i = 0; i < 6; i++) pieceCounts[i] = 0;
    }

    void fillPieceCounts(){
        for(int i = 0; i < pieceCount; i++){
            if(!pieceSetExplored[i]) {
                switch(pieceSet[i]){
                case 'p':
                    pieceCounts[0]++;
                    break;
                case 'b':
                    pieceCounts[1]++;
                    break;
                case 'z':
                    pieceCounts[2]++;
                    break;
                case 'r':
                    pieceCounts[3]++;
                    break;
                case 'v':
                    pieceCounts[4]++;
                    break;
                case 'k':
                    pieceCounts[5]++;
                    break;
                }
            }
        }

    }

    bool firstCheck(){
        if(pieceCounts[0] < 16) return false;
        if(pieceCounts[1] < 4) return false;
        if(pieceCounts[2] < 4) return false;
        if(pieceCounts[3] < 4) return false;
        if(pieceCounts[4] < 2) return false;
        if(pieceCounts[5] < 2) return false;
        return true;
    }

    void readFile(string s){
        refreshPieceCounts();
        ifstream fi(s);
        fi >> pieceCount;
        for(int i = 0; i < pieceCount; i++){
            fi >> pieceSet[i];
        }
        fi.close();
    }

    bool explore(int x, int p, int b, int z, int r, int v, int k){
        if(pieceSetExplored[x]) return false; // jei jau paimtas
        if(p == 0 && b == 0 && z == 0 && r == 0 && v == 0 && k == 0) // jei atrado viena rinkini
            return true;
        //cout << p << " " << b << " " << z << " " << r << " " << v << " " << k << endl;

        pieceSetExplored[x] = true;
        if(p != 0 && pieceSet[x] == 'p' && explore(x + 1, p - 1, b, z, r, v, k)) return true;
        if(b != 0 && pieceSet[x] == 'b' && explore(x + 1, p, b - 1, z, r, v, k)) return true;
        if(z != 0 && pieceSet[x] == 'z' && explore(x + 1, p, b, z - 1, r, v, k)) return true;
        if(r != 0 && pieceSet[x] == 'r' && explore(x + 1, p, b, z, r - 1, v, k)) return true;
        if(v != 0 && pieceSet[x] == 'v' && explore(x + 1, p, b, z, r, v - 1, k)) return true;
        if(k != 0 && pieceSet[x] == 'k' && explore(x + 1, p, b, z, r, v, k - 1)) return true;

        pieceSetExplored[x] = false;
        if(explore(x + 1, p, b, z, r, v, k)) return true;

        return false;
    }

 int main(){

    readFile("U3.txt");
    fillPieceCounts();
    ////I ekrana
    cout << "p " << pieceCounts[0] << endl;
    cout << "b " << pieceCounts[1] << endl;
    cout << "z " << pieceCounts[2] << endl;
    cout << "r " << pieceCounts[3] << endl;
    cout << "v " << pieceCounts[4] << endl;
    cout << "k " << pieceCounts[5] << endl;
    //////
    bool b = firstCheck();
    refreshPieceCounts();

    int full = 0;
    if(b){
        for(int i = 0; i < pieceCount; i++){
            bool b = explore(i, 16, 4, 4, 4, 2, 2);
            if(b) full++;
        }
    }
    fillPieceCounts();
    cout << full << endl;
    ////I ekrana
    cout << "p " << pieceCounts[0] << endl;
    cout << "b " << pieceCounts[1] << endl;
    cout << "z " << pieceCounts[2] << endl;
    cout << "r " << pieceCounts[3] << endl;
    cout << "v " << pieceCounts[4] << endl;
    cout << "k " << pieceCounts[5] << endl;
    //////

 return 0; }

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

    struct Shortcut{
        int a, b;
    } shortcuts[51];

    int width, shortCount, maxUPI = 0, maxDOWNI = 0;
    int UPC = 0, DOWNC = 0;

    void readFile(string s){
        int tmpMaxU = 0;
        int tmpMaxD = 0;
        ifstream fi(s);
        fi >> width >> shortCount;

        for(int i = 0; i < shortCount; i++){
            int a, b; fi >> a >> b;
            shortcuts[i].a = a; shortcuts[i].b = b;
            if(a < b){ // jei kopiecios
                 if(tmpMaxU > a - b) { tmpMaxU = a - b; maxUPI = i; }
                 UPC++;
            }else{
                 if(tmpMaxD < a - b) { tmpMaxD = a - b; maxDOWNI = i; }
                 DOWNC++;
            }
        }

        fi.close();
    }

 int main(){

    readFile("U2.txt");
    cout << "Kopeciu skaicius: " << UPC << endl;
    cout << "Zalciu skaicius: " << DOWNC << endl;
    cout << "Didziausias pakilimas: " << shortcuts[maxUPI].a << " - "<< shortcuts[maxUPI].b << endl;
    cout << "Didziausias nusileidimas: " << shortcuts[maxDOWNI].a << " - "<< shortcuts[maxDOWNI].b << endl;

    ////kopecios
    cout << "Kopecios: " << endl;
    for(int i = 0; i < shortCount; i++){
        int a = shortcuts[i].a, b = shortcuts[i].b;
        if(a - b < 0){
            cout << a << " - " << b << ": " << ceil((double)a / width)
             << " - " <<  ceil((double)b / width) << endl;
        }
    }

    ////zalciai
    cout << "Zalciai: " << endl;
    for(int i = 0; i < shortCount; i++){
        int a = shortcuts[i].a, b = shortcuts[i].b;
        if(a - b > 0){
            cout << a << " - " << b << ": " << ceil((double)a / width)
             << " - " <<  ceil((double)b / width) << endl;
        }
    }

 return 0; }

#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
using namespace std;

    #define FILE "U1.txt"

    void processNumberCount(int n, int k){
        int numberBuffer[10]; fill(numberBuffer, numberBuffer + 10, 0);
        for(int floor = 1; floor <= n; floor++){
            for(int room = 1; room <= k; room++){
                numberBuffer[room / 10]++;
                numberBuffer[room % 10]++;
                numberBuffer[floor]++;
            }
        }
        for(int i = 0; i < 10; i++)
            cout << setw(4) << numberBuffer[i];
        cout << endl;
    }

 int main(){

    ifstream FI(FILE); int N; FI >> N;
    cout << setw(10) << "Skaitmenys: " << setw(4) << "0" << setw(4) << "1" << setw(4) << "2" << setw(4)
    << "3" << setw(4) << "4" << setw(4) << "5" << setw(4) << "6" << setw(4) << "7" << setw(4)
    << "8" << setw(4) << "9" << endl;

    cout << setw(52) << "-----------------------------------------" << endl;
    for(int i = 1; i <= N; i++){
        int a, b; FI >> a >> b;
        cout << i << " pastatas: ";
        processNumberCount(a, b);
    }
    FI.close();

 return 0; }

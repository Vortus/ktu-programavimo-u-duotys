#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>

using namespace std;

    string words[101];
    int wordCount;

    bool isAnagram(string of, string a){
        sort(of.begin(), of.end());
        sort(a.begin(), a.end());
        return of.compare(a) == 0 ? true : false;
    }

    void readFile(string s){
        ifstream fi(s);
        fi >> wordCount;
        for(int i = 0; i < wordCount; i++)
            fi >> words[i];
        fi.close();
    }

 int main(){

    readFile("U2.txt");

    for(int i = 0; i < wordCount; i++){
        if(words[i] != "-1"){
            cout << words[i];
            for(int j = i + 1; j < wordCount; j++){
                if(words[j] != "-1" && isAnagram(words[i], words[j])){
                cout << ", " << words[j];
                words[j] = "-1";
                }
            }
            cout << "." << endl;
       }
    }


 return 0; }

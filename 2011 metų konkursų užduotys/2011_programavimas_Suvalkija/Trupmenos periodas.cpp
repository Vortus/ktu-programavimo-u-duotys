#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    void displayPeriodic(int a, int b){
        string periodic;
        int times = a / b, remainder = a % b;
        int first = remainder * 10;
        cout << times << ".";
        bool stop = false, firstb = true;
        for(int i = 0; i < 12; i++){
            a = remainder * 10;
            if(!stop && !firstb && a == first)
                stop = true;
            times = a / b; remainder = a % b;
            if(!stop){
                firstb = false;
                periodic += (times + '0');
            }
            if(i == 11 && !stop) times++;
            cout << times;
        }

        cout << setw(13) << right;
        if(!stop && b % 3 != 0){ cout << 0 << endl; return; } // jei nera isvis
        if(b % 3 != 0) cout << periodic << endl;
        else cout << periodic[periodic.length() - 2] << endl;

    }

    void readFile(string s){
        ifstream fi(s);
        int n; fi >> n;
        for(int i = 0; i < n; i++){
            int a, b; fi >> a >> b;
            displayPeriodic(a, b);
        }
        fi.close();
    }

 int main(){

    readFile("U1.txt");

 return 0; }

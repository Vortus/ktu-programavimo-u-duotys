#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int sw = 3, sh = 2; // dimensija bloko
    int ns = 6; // lentos dydis
    int board[6][6];

    bool findUnassigned(int &x, int &y){
        for(y = 0; y < ns; y++)
            for(x = 0; x < ns; x++)
                if(board[y][x] == 0) return true;
        return false;
    }

    bool checkVertical(int x, int num){
        for(int y = 0; y < ns; y++)
            if(board[y][x] == num) return true;
        return false;
    }

    bool checkHorizontal(int y, int num){
        for(int x = 0; x < ns; x++)
            if(board[y][x] == num) return true;
        return false;
    }

    bool checkBox(int bsx, int bsy, int num){
        for(int y = 0; y < sh; y++)
            for(int x = 0; x < sw; x++)
                if(board[y + bsy][x + bsx] == num) return true;
        return false;
    }

    bool isSafe(int x, int y, int num){
        return !checkVertical(x, num) &&
                !checkHorizontal(y, num) &&
                !checkBox(x - x % sw, y - y % sh, num);
    }

    bool solveSudoku(){
        int x, y;
        if(!findUnassigned(x, y))
            return true;

        for(int i = 1; i <= ns; i++){
            if(isSafe(x, y, i)){
                board[y][x] = i;
                if(solveSudoku())
                    return true;
                board[y][x] = 0;
            }
        }
        return false;
    }

    void readFile(string s){
        ifstream fi(s);
        for(int i = 0; i < ns; i++)
            for(int j = 0; j < ns; j++)
                fi >> board[i][j];
        fi.close();
    }

 int main(){

    readFile("U3.txt");
    solveSudoku();

    for(int i = 0; i < ns; i++){
        for(int j = 0; j < ns; j++){
            cout << board[i][j];
        }
        cout << endl;
    }

 return 0; }

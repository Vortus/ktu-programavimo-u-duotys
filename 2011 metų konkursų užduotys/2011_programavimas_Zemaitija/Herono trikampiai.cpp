#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

    double area(double a, int b, int c){
        double p = (a + b + c) / 2;
        return sqrt(p * (p - a) * (p - b) * (p - c));
    }

 int main(){

    int n; cin >> n;
    cout << setw(5) << "a" << setw(5) << "b" << setw(5) << "c" << setw(5) << "S" << endl;
    cout << "--------------------------" << endl;

    for(int a = 1; a < n; a++)
       for(int b = 1; b < a; b++)
           for(int c = 1; c < b; c++)
               if(a * a == (b * b + c * c))
               cout << setw(5) << a << setw(5) << b << setw(5) << c << setw(5) << area(a, b, c) << endl;

 return 0; }

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    void check(int a, int b, int c, int d){
        cout << a << " " << b << " " << c << " " << d << " ";
        int x = a + c, y = b + d; // /2 reikia
        int w = c - a, h = b - d;
        if(w != h){ cout << "ne kvadratas" << endl; return; }
        if(x % 2 != 0 || y % 2 != 0){ cout << "realusis" << endl; return; }
        cout << x / 2 << " " << y / 2 << endl;
    }

    void readFile(string s){
        ifstream fi(s);
        int n; fi >> n;
        for(int i = 0; i < n; i++){
            int a, b, c, d; fi >> a >> b >> c >> d;
            check(a, b, c , d);
        }
        fi.close();
    }

 int main(){

    readFile("U1.txt");

 return 0; }

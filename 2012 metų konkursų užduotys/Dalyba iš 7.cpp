#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int n;

    void readFile(string s){
        ifstream fi(s);
        fi >> n;
        fi.close();
    }

    void calculateAndReturn(){
        ofstream fo("KU1rez.txt");
        while(n > 9){
            int j = n % 10;
            n /= 10;
            n -= 2 * j;
            fo << n << endl;
        }

        if(n % 7 == 0) fo << "Dalinasi";
        else fo << "Nesidalina";
        fo.close();
    }

 int main(){

    readFile("KU1.txt");
    calculateAndReturn();

 return 0; }

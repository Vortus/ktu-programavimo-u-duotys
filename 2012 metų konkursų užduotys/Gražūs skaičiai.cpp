#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

    void readFile(string s, int &n){
        ifstream fi(s);
        fi >> n;
        fi.close();
    }

    bool check(int n){
        int sum = 0, mult = 1;
        while(n > 0){
            int j = n % 10;
            n /= 10;
            sum += j;
            mult *= j;
        }
        return sum == mult ? true : false;
    }

 int main(){

    int n = 1;
    readFile("KU2.txt", n);
    int maxN = pow(10, n);
    int minN = pow(10, n - 1);

    for(int i = minN; i <= maxN; i++)
        if(check(i)) cout << i << " ";

 return 0; }

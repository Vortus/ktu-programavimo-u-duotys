#include <iostream>
#include <iomanip>
#include <fstream>
#define VISITED 2
#define AVAILABLE 1

using namespace std;

    int w, h; // aukstis plotis
    int map[1000][1000]; // mapas
    int wallMap[1000][1000]; // sienos
    int costs[4];

    void readFile(string s){

        costs[0] = 1;
        costs[1] = 2;
        costs[2] = 4;
        costs[3] = 8;

        ifstream fi(s);
        fi >> h >> w;
        for(int i = 0; i < h; i++)
            for(int j = 0; j < w; j++){
                fi >> wallMap[i][j];
                map[i][j] = AVAILABLE;
            }
        fi.close();
    }

    bool explore(int startX, int startY){
        if(map[startY][startX] == VISITED) return false;
        map[startY][startX] = VISITED; // uzlipa

        int sum = 15;
        int index = 3;

        while(sum >= wallMap[startY][startX]){
            if(sum == wallMap[startY][startX]) break;
            if(sum - costs[index] < wallMap[startY][startX]) index--;
            else{
            sum -= costs[index];
            if(startX + 1 != w && costs[index] == 4) explore(startX + 1, startY);
            else if(startX != 0 && costs[index] == 1) explore(startX - 1, startY);
            else if(startY != 0 && costs[index] == 2) explore(startX, startY - 1);
            else if(startY + 1 != h && costs[index] == 8) explore(startX, startY + 1);
            }
        }
    }

 int main(){

    readFile("ZU3.txt");

    int c = 0;
    for(int i = 0; i < h; i++){
        for(int j = 0; j < w; j++){
            bool b = explore(j, i);
            if(b) c++;
        }
    }
    for(int i = 0; i < h; i++){
        for(int j = 0; j < w; j++)
            cout << setw(3) << wallMap[i][j];
        cout << endl;
    }


    cout << endl << "Kambariu kiekis: " << c;

 return 0; }

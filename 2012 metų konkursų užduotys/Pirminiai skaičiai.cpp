#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

    bool prime(int n){
        for(int i = 2; i <= sqrt(n); i++)
            if(n % i == 0) return false;
        return true;
    }

    void readFile(string s, int &n){
        ifstream fi(s);
        fi >> n;
        fi.close();
    }

 int main(){

    int n;
    readFile("ZU1.txt", n);
    bool exist = false;
    ofstream fo("ZU1rez.txt");
    for(int i = 2; i <= n / 2; i++){
        int b = n - i;
        if(prime(i) && prime(b) && i + b == n){
            fo << n << " = " << i << " + " << b << endl;
            if(!exist) exist = true;
        }
    }

    if(!exist) fo << n << " = NE";
    fo.close();

 return 0; }

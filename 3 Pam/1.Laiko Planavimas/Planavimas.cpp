#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    #define MAXA 1000 //maks veiksmu

    struct Action{
        int plannedTime, doneTime;// suplanuotas laikas, atliktas laikas
        void initialize(int pt, int dt){ plannedTime = pt; doneTime = dt; } //kad patogiau butu
    } actions[MAXA]; // veiksmu strukturu masyvas

    int NN = 0; // kiek yra vykditu uzduociu (tie kur nera 0)
    int N = 0; // kiek yra isviso uzduociu

    int getAllPlanned(){ // visas susiplanuotas
        int result = 0;
        for(int i = 0; i < N; i++)
            result += actions[i].plannedTime;
        return result;
    }

    int getAllDone(){ // visas padarytas
        int result = 0;
        for(int i = 0; i < N; i++)
            result += actions[i].doneTime;
        return result;
    }

    double averageDone(double allD){ // vidutiniskai
        return allD / NN;
    }

    void readFile(string s){ // failo nuskaitymas
        ifstream fi(s);
            while(!fi.eof()){
                fi.ignore(256, ','); // vardo net nereikia
                int a, b; fi >> a >> b; if(b != 0) NN++; // jei vykdoma uzduotis
                actions[N++].initialize(a, b);
            }
        fi.close();
    }

    void outputResults(string s){ // failo isvedimas
        ofstream fo(s);
        int p = getAllPlanned(), d = getAllDone();
        fo << "I� viso Petriukas buvo suplanav�s u�trukti " << p << " minutes" << endl;
        fo << "I� viso Petriukas u�trukto " << d << " minutes" << endl;
        fo << "Petriukas vykd� " << NN << " u�duotis, joms vidutini�kai skyr� po " << averageDone(d) << " minutes" << endl;
        fo.close();
    }

 int main(){

    readFile("Duomenys1.txt");
    outputResults("Rezultatai1.txt");

 return 0; }

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    #define MAXS 1000 // maks dainu

    struct Song{ // kadangi vardo nereikia tai ir nededam
        string author; int mins, secs;
        void initialize(string a, int m, int s){ // pagalbine funkcija
            author = a; mins = m; secs = s; }
    } songs[MAXS]; // strukturu masyvas

    int N; // viso atlikeju
    int NN = 0; // rastu atlikeju
    int allMins = 0; // visos minutes
    string author; // ieskomas atlikejas

    int averageTime(int all){ // vidutiniskai visu min
        return all / N;
    }

    void getListeningTime(int &result){ // kiek uztruks perklausyti pasirinktas
        for(int i = 0; i < N; i++)
            if(songs[i].author == author)
                result += songs[i].mins * 60 + songs[i].secs;
    }

    void readFile(string s){ // failo nuskaitymas
        ifstream fi(s); fi >> N; // visi
        for(int i = 0; i < N; i++){
            string aut; int m, s; char c; fi.get(c);
            while(c != '-'){ // paema simbolius kuriu reikia
               if(c != '\n') aut += c; fi.get(c); // pasiemam
            }
            fi.ignore(256, ','); // nereikia pavadinimo dainos
            fi >> m >> s; if(aut == author) NN++; // jei radom autoriu
            songs[i].initialize(aut, m, s);
            allMins += m * 60 + s;
        }
        fi.close();
    }

    void outputFile(string s){ // isvedimas
        ofstream fo(s); int all = 0; getListeningTime(all);
        int av = averageTime(allMins); // vidutiniskas
        fo << "Atlikejo " << author << " dain� kiekis: " << NN << endl;
        fo << "Jas perklausyti u�truks " << all / 60 << "m " << all % 60 << "s" << endl;
        fo << "Vis� dain� trukm�s vidurkis: " << av / 60 << "m " << av % 60 << "s" << endl;
        fo.close();
    }

 int main(){

    getline(cin, author);
    readFile("Dainos.txt");
    outputFile("Rezultatai.txt");

 return 0; }

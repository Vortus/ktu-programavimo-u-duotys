#include <iostream>
#include <iomanip>
#include <vector>
#include <fstream>
using namespace std;

    /*
        Kadangi nenorim kad naudojant struktus kiekvienas turetu sau visiskai atskira knygu sarasa
        ir nebutu bereikalingai atidaroma vieta atmintyje, naudojam pointerius.
        Aisku galima talpinti knygu indeksus, bet su pointeriais greiciau veikia :P
    */

    #define MAXB 1000 // maks knygu
    #define MAXC 1000 //maks klientu

    struct Book{ // knygos duomenys
        string ID, info; double cost; bool bought;
        void output(ofstream &fo){ fo << ID << " " << info << " " << cost << endl; }
        void initialize(string id, string in, double c){ ID = id; info = in; cost = c; }
    };

    struct Client{ // kliento duomenys
        string name;
        Book *queuedBooks[MAXB];
        Book *boughtBooks[MAXB];
        int N = 0, NN = 0; double money; // queueBooks dydis, bought dydys, pinigai
        void initialize(string n, double m){ name = n; money = m; }
    };

    struct Shop{ // knygas sauganti struktura
        Book books[MAXB]; int N;
    } Shop;

    struct ClientQueue{ //klijentu eile
        Client clients[MAXC]; int N;
    } ClientQueue;

    bool bookExists(string ID){ // ar knyga egzistuoja
        for(int i = 0; i < Shop.N; i++)
            if(Shop.books[i].ID == ID) return true;
        return false;
    }

    bool canBuy(Book *b, Client c){ // ar gali nusipirkt
        return c.money >= b -> cost && !(b -> bought) ? true : false;
    }

    Book *getBook(string ID){ // gauni knyga
        for(int i = 0; i < Shop.N; i++)
            if(Shop.books[i].ID == ID) return &Shop.books[i];
    }

    void removeBook(Book *b){ // salina knyga
        b -> bought = true;
    }

    void buyBooks(){
        for(int i = 0; i < ClientQueue.N; i++){ // per visus klientus
            for(int j = 0; j < ClientQueue.clients[i].N; j++){ // kliento pageidavimai
                Book *b = ClientQueue.clients[i].queuedBooks[j];
                if(canBuy(b, ClientQueue.clients[i])){
                    b -> bought = true;
                    ClientQueue.clients[i].boughtBooks[ClientQueue.clients[i].NN++] = b;
                    ClientQueue.clients[i].money -= b -> cost;
                }
            }
        }
    }

    void readBooks(string s){ // knygu nuskaitymo funkcija
        ifstream fi(s); fi >> Shop.N;
        for(int i = 0; i < Shop.N; i++){
            string id, a, b, d; double c; // id info cost aisku sitie kintamieji nebutini
            fi >> id >> a >> b >> d >> c;
            Shop.books[i].initialize(id, a + " " + b + " " + d, c);// sudedam i parduotuve knygas
        }
        fi.close();
    }

    void readClients(string s){ // klijentu nuskaitimo funkcija
        ifstream fi(s); fi >> ClientQueue.N;
        for(int i = 0; i < ClientQueue.N; i++){
            string name; double money; int n; //kadangi esu priprates prie java garbage collectoriaus aisku blogai kiekviena kart kurti nauja
            fi >> name >> money >> n;
            for(int j = 0; j < n; j++){ // uzqueuinti knygas
                string id; fi >> id;
                if(bookExists(id)){ // jei tokia knyga egzistuoja
                    Book *b = getBook(id);
                    ClientQueue.clients[i].queuedBooks[ClientQueue.clients[i].N++] = b;
                }
            }
            ClientQueue.clients[i].initialize(name, money);
        }
        fi.close();
    }

    void outputResults(){ // ispauzdinti rezultatus
        ofstream fo("Rezultatai.txt");
        for(int i = 0; i < ClientQueue.N; i++){
            fo << ClientQueue.clients[i].name << endl;
            fo << "------------------------------------------" << endl;
            for(int j = 0; j < ClientQueue.clients[i].NN; j++)
                ClientQueue.clients[i].boughtBooks[j] -> output(fo);
            fo << "------------------------------------------" << endl;
        }
        fo.close();
    }

 int main(){

    readBooks("Knygos.txt");
    readClients("Pirkejai.txt");
    buyBooks();
    outputResults();

 return 0; }

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    #define MAXP 1000 //maks zaideju

    struct Banknote{ // banknotas
        int value, available; // reiksme, kiek liko
    } banknotes[10]; // kupiuros

    struct Player{
        string name; int needMoney; // vardas, kiek reik atiduot
        int pocket[7]; // paimti banknotai
        void initialize(string n, int nm){
            fill(pocket, pocket + 7, 0); name = n; needMoney = nm; }
    } players[MAXP];

    int N; // zaideju skaicius

    int fitBanknote(Banknote *bn, Player *p){ // kiek reikia duoti banknotu
        int j = p -> needMoney / bn -> value; //
        return bn -> available - j > -1 ? j : bn -> available; // jei neuztenka banknotu
    }

    void executeTransfer(bool &can, Player *p){ // ta funkcija su boolu...
        can = false;
        for(int i = 0; i < 7; i++){ // per visus banknotus
            int j = fitBanknote(&banknotes[i], p); // kiek max paimt galima
            banknotes[i].available -= j; // paemi
            p -> needMoney -= j * banknotes[i].value; //atemi is zaidejo pinigus
            p -> pocket[i] = j; // idedi i zaidejo "kisene"
            // optimizavimai
            if(p -> needMoney == 0) { can = true; break; }
            if(p -> needMoney < 0) { can = false; break; }
        }
        if(!can){ // jei negali sudaryt tai
            for(int i = 0; i < 7; i++) // atgrazinam atgal zaidejo paimtus
                banknotes[i].available += p -> pocket[i];
        }
    }

    void readFile(string s){// failo nuskaitymas
        ifstream fi(s);
        for(int i = 0; i < 7; i++) fi >> banknotes[i].value; // banknotu reiksmes
        for(int i = 0; i < 7; i++) fi >> banknotes[i].available; // kiekiai
        fi >> N; // zaidejai
        for(int i = 0; i < N; i++){
            string name; int c; fi >> name >> c;
            players[i].initialize(name, c);
        }
        fi.close();
    }

    void outputFile(string s){
        ofstream fo(s); bool b;
        int ns = 0, nsum = 0; // likes kiekis ir suma
        for(int i = 0; i < N; i++){ // per visus zaidejus
            executeTransfer(b, &players[i]); // bandom duot pinigus
            fo << players[i].name << " ";
            if(b){ // jei isejo spausdinam kisene
                for(int j = 0; j < 7; j++)
                    fo << players[i].pocket[j] << " ";
                fo << endl;
            }else{
                fo << "NEGALIMA" << endl;
            }
        }
        for(int i = 0; i < 7; i++){
            ns += banknotes[i].available;
            nsum += banknotes[i].available * banknotes[i].value;
        }
        fo << ns << " " << nsum;
        fo.close();
    }

 int main(){

    readFile("Duomenys.txt");
    outputFile("Rezultatai.txt");

 return 0; }

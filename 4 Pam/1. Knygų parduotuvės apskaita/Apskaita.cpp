#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
using namespace std;

    #define FILE "Duomenys1.txt"
    #define FILEOUT "Rezultatai1.txt"
    #define MAXN 1000

    struct Day{
        int ID; // diena, parduota, pajamos
        pair<double, double> data;
        void init(int a, double b, double c){
            ID = a; data.first = b; data.second = c; }
        bool operator <(const Day &a) const{ // sortas pagal id
            return ID < a.ID;
        }
    } days[MAXN];

    bool dayCompare(Day a, Day b){ // palyginimas dienu
        return a.data.second < b.data.second ||
        (!(b.data.second < a.data.second) && a.data.first < b.data.first);
    }

    int N = 0, NN = 0; // dienu kiekis, pelningu dienu kiekis
    double average; // vidurkis

    Day *best(){ return &days[N - 1]; } // pelningiausia
    Day *worst(){ return &days[0]; }  // nepelningiausia

    Day *incomeDays[MAXN]; // pelno dienu masyvas

    void readFile(){ // failo nuskaitymas
        ifstream FI(FILE);
        double a, b, c;
        while(FI >> a >> b >> c)
            days[N++].init(a, b, c);
        FI.close();
    }

    void displayDiagram(ofstream &FO){ // diagrama
        /*
            Sprenimas, uzsipildyt masyva simboliu, ir kur reikia iterpt skyriklius
        */
        //pradzia
        if(N == 0) return; int breakPoint = days[0].data.second, cc;
        FO << "          Pajamu_Diagrama" << endl;
        FO << "Dienos" << endl;
        FO << setw(6);
        for(int i = 0; i < breakPoint; i++) FO << "_"; FO << endl;

        //vidurys
        for(int i = 1; i < N; i++){
            FO << setw(5) << left << days[i - 1].ID;
            cc = i == 0 ? days[i].data.second : (max(days[i].data.second, days[i - 1].data.second)); // linijos ilgis
            string s; for(int j = 0; j < cc; j++) s += "_"; // uzpildom masyva linija
            //iterpiami skirikliai
            s[days[i - 1].data.second] = '|';
            if(cc == days[i - 1].data.second) s += '|';
            ////
            FO << s << endl;
        }

        //pabaiga
        if(N == 1) return; // jei ne vieno dydzio testi
        FO << setw(5) << left << days[N - 1].ID;
        for(int i = 0; i < days[N - 1].data.second; i++)
            FO << "_";
        FO << "|" << endl;
    }

    void fillIncomeDays(){ // uzpildomas pelno dienu masyvas
        for(int i = 0; i < N; i++)
            if(days[i].data.second >= average)
                incomeDays[NN++] = &days[i];
    }

    void displayBoard(ofstream &FO){ // lentele
        FO << endl << "                  Pelningos_Dienos" << endl << endl;
        FO << left << "|---------------" << "|---------------" << "|---------------|" << endl;
        FO << setw(16) << "|     Diena" << setw(16) << "|    Knygos" << setw(16) << "|    Pajamos" << "|" << endl;
        FO << left << "|---------------" << "|---------------" << "|---------------|" << endl;
        for(int i = 0; i < NN; i++){
            FO << "|" <<  setw(15) << incomeDays[i] -> ID << "|";
            FO << setw(15) << incomeDays[i] -> data.first << "|";
            FO << setw(15) << incomeDays[i] -> data.second << "|" << endl;
        }
        FO << "|---------------" << "|---------------" << "|---------------|" << endl;
    }

 int main(){

    readFile(); // faila nuskaitom
    sort(days, days + N, dayCompare); // patogiau rast min max
    average = (best() -> data.second + worst() -> data.second) / 2; // vidutinis
    sort(days, days + N); // atgal pagal id
    fillIncomeDays(); // uzpildom pelno dienas

    ofstream FO(FILEOUT);
    displayDiagram(FO);
    displayBoard(FO);
    FO.close();

 return 0; }

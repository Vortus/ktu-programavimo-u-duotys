#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
using namespace std;

    #define FILE "Duomenys2.txt"
    #define FILEOUT "Rezultatai2.txt"
    #define MAXN 1000

    struct Contestant{
        string name, surname;
        int points, completed;
        void init(string a, string b, int c, int d ){
            name = a; surname = b; points = c; completed = d;}
    } contestants[MAXN];

    bool contestantCompare(Contestant a, Contestant b){ // lyginimas su lygintuvu
        return a.completed < b.completed ||
        (!(b.completed < a.completed) && a.points < b.points);
    }

    int N; // dalyviu kiekis
    Contestant *exclusiveContestants[4]; //  masyvas rezultatu

    Contestant *worst(){ // blogiausiai pasirodziusio dalyvio suradimas
        return &contestants[0];
    }

    void fillExclusiveContestants(){ // masyvo uzplidymo funkcija
        for(int i = 0; i < 3; i++)
            exclusiveContestants[i] = &contestants[N - i - 1];
        exclusiveContestants[3] = worst();
    }

    void readFile(){ // failo nuskaitymas
        ifstream FI(FILE); FI >> N;
        string n, s; int a = 0, b = 0, k = 0, i = 0, j = 0;
        for(i = 0; i < N; i++){
            FI >> n >> s; a = 0; b = 0;
            for(int j = 0; j < 5; j++){
                FI >> k; a += k;
                if(k == 100) b++; // jei maks
            }
            contestants[i].init(n, s, a, b);
        }
        FI.close();
    }

    void displayResults(){ // rezultatu isveidmas
        ofstream FO(FILEOUT);
        FO << "Konkurso prizininkai: " << endl;
        for(int i = 0; i < 4; i++){
            if(i == 3) FO << "Prasciausiai pasirode: " << endl;
            FO << exclusiveContestants[i] -> name << " ";
            FO << exclusiveContestants[i] -> surname << " ";
            FO << exclusiveContestants[i] -> points << " ";
            FO << exclusiveContestants[i] -> completed << endl;
        }
        FO.close();
    }

 int main(){

    readFile(); // nuskaitom faila
    sort(contestants, contestants + N, contestantCompare); // surikiuojam
    fillExclusiveContestants(); // uzpildom rezultatu masyva
    displayResults(); // pateikiam rezultatus

 return 0; }

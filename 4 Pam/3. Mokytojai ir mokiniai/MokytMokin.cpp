#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    #define FILEMOKYT "Mokytojai.txt"
    #define FILEMOKIN "Mokiniai.txt"
    #define FILEOUT "Rezultatai.txt"
    #define MAXN 1000

    struct Teacher{
        string name, surname, study;
        int N; // mokiniu kiekis
        double pointSum;
        void init(string a, string b, string c){ name = a; surname = b; study = c; }
    } teachers[MAXN];

    int N; // mokytoju kiekis

    Teacher *getTeacherByStudy(string s){ // funkcija grazinanti mokytoja pagal pamoka
        for(int i = 0; i < N; i++)
            if(teachers[i].study == s) return &teachers[i];
        return NULL;
    }

    void readTeachersFile(){ // mokytoju failo nuskaytimas
        ifstream FI(FILEMOKYT); FI >> N;
        string a, b, c;
        for(int i = 0; i < N; i++){
            FI >> a >> b >> c;
            teachers[i].init(a, b, c);
        }
        FI.close();
    }

    void proccessPupils(){ // apdoroti mokiniu faila
        int NN, k; string a, b, c; Teacher *tmp;
        ifstream FI(FILEMOKIN); FI >> NN;
        for(int i = 0; i < NN; i++){
            FI >> a >> b >> c >> k;
            tmp = getTeacherByStudy(c);
            tmp -> N++;
            tmp -> pointSum += k;
        }
        FI.close();
    }

    void displayResults(){ // rezultatu isvedimas
        ofstream FO(FILEOUT);
        Teacher *most = &teachers[0], *best = &teachers[0]; // daugiausia, geriausi
        int mostN = 0; double bestAv = 0;
        for(int i = 0; i < N; i++){
            FO << teachers[i].surname << " " << teachers[i].N << endl;
            if(mostN < teachers[i].N){ mostN = teachers[i].N; most = &teachers[i]; }
            if(bestAv < (teachers[i].pointSum / teachers[i].N)) { bestAv = (teachers[i].pointSum / teachers[i].N); best = &teachers[i]; }
        }
        FO << "Daugiausia mokini� turi: " << most -> name << " " << most -> surname << endl;
        FO << "Geriausiai mokiniai mokosi pas: " << best -> name << " " << best -> surname << endl;
        FO.close();
    }

 int main(){

    readTeachersFile(); // nuskaitoms mokytoju failas
    proccessPupils(); //apdorojam mokinius
    displayResults(); //isvedam rezultatus

 return 0; }
//69 Kappa

#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <cmath>
using namespace std;

    #define FILE "klasePAGR.txt"
    #define FILEOUT "pamokos.txt"
    #define MAXN 1000

    struct DATA { // mokinio/pamokos struktura
        string* name;
        string* knows[MAXN];
        string* knowsBad[MAXN];
        int N = 0, NN = 0;// gerai, negerai
    } students[MAXN], lessons[MAXN];

    DATA know, knowBad, difference;
    string lessonNames[MAXN]; // galimi pamoku pavadinimai
    string studentNames[MAXN]; // mokiniu vardai
    int N, NL = 0; // mokiniu kiekis, pamoku kiekis

    int findLessonName(string n){ // ieskoti ar yra vardas
        for(int i = 0; i < NL; i++)
            if(n == lessonNames[i]) return i;
        return -1;
    }

    void fillLessons(){ // performavimo funkcija
        int i, j, k;
        for(i = 0; i < NL; i++){ // per visas pamokas
            lessons[i].name = &lessonNames[i];
            for(j = 0; j < N; j++){ // per visus mokinius
                for(k = 0; k < students[j].N; k++){ // zino
                    if(lessonNames[i] == *students[j].knows[k])
                        lessons[i].knows[lessons[i].N++] = students[j].name;
                }
                for(k = 0; k < students[j].NN; k++){ // nezino
                    if(lessonNames[i] == *students[j].knowsBad[k])
                        lessons[i].knowsBad[lessons[i].NN++] = students[j].name;
                }
            }
            if(lessons[i].N > know.N) know = lessons[i];
            if(lessons[i].NN > know.NN) knowBad = lessons[i];
            int diff = abs(lessons[i].NN - lessons[i].N);
            if(diff > abs(difference.NN - difference.N)) difference = lessons[i];
        }

    }

    void readFile(){ // failo nuskaitymas
        ifstream FI(FILE); FI >> N;
        int k;  string n; FI.ignore();
        for(int i = 0; i < N; i++){
            getline(FI, n);
            studentNames[i] = n; students[i].name = &studentNames[i];
            FI >> k; FI.ignore();
            for(int j = 0; j < k; j++){ //moka
                getline(FI, n);
                if(findLessonName(n) == -1) lessonNames[NL++] = n; // jei nera elemento pridedam i sarasa
                students[i].knows[students[i].N++] = &lessonNames[findLessonName(n)];
            }
            FI >> k; FI.ignore();
            for(int j = 0; j < k; j++){ //nemoka
                getline(FI, n);
                if(findLessonName(n) == -1) lessonNames[NL++] = n; // jei nera elemento pridedam i sarasa
                students[i].knowsBad[students[i].NN++] = &lessonNames[findLessonName(n)];
            }
        }
        FI.close();
    }

    void displayTopTable(ofstream &FO, string a, string b, string c){
        FO << "---------------------------------------------------------------------------------" << endl;
        FO << left << "| " << setw(24) << a << " | " << setw(24) << b << " | " << setw(24) << c << " |" << endl;
        FO << "---------------------------------------------------------------------------------" << endl;
    }

    void displayTable(ofstream &FO, string aa, string bb, string cc, DATA *d, int n){
        displayTopTable(FO, aa, bb, cc);
        int j, i, lineC;
        for(i = 0; i < n; i++){
            lineC = max(d -> N, d -> NN);
            string tmpN = *d -> name;
            for(j = 0; j < lineC; j++){
                string a, b;
                if(d -> N > j) a = *d -> knows[j]; // ribojimai kad neuzkrasintu
                if(d -> NN > j) b = *d -> knowsBad[j];// ribojimai kad neuzkrasintu
                FO << right << "| " << setw(24) << tmpN << " | " << setw(24) << a << " | " << setw(24) << b << " |" << endl;
                if(j == 0) tmpN = ""; // kai vardas ispausdintas paresetint
            }
            FO << "---------------------------------------------------------------------------------" << endl;
            d++;
        }
        FO << endl << endl;
    }

    void finding(string &a, string &b, string &c){ // min max skirtumas
        for(int i = 0; i < NL; i++){
            if(lessons[i].N == know.N){ a += ", " + *lessons[i].name; }
            if(lessons[i].NN == knowBad.NN){ b += ", " + *lessons[i].name; }
            if(abs(lessons[i].NN - lessons[i].N) == abs(difference.NN - difference.N)){ c += ", " + *lessons[i].name; }
        }
    }

    void displayResults(ofstream &FO){
        string a, b, c; finding(a, b, c);
        FO << know.N << " supranta" << a << "." << endl;
        FO << knowBad.NN << " nesupranta" << b << "." << endl;
        FO << "Didziausias skirtumas yra" << c << "; skirtumas " << abs(difference.NN - difference.N) << " mokiniai" << endl;
    }

 int main(){

    readFile(); // failo nuskaitymas
    fillLessons(); // uzpildom masyva
    ofstream FO(FILEOUT);
    displayTable(FO, "Mokinys", "Puikiai moka", "Nemoka", students, N); // mokinu lentele
    displayTable(FO, "Pavadinimas", "Mokantys mokiniai", "Nemokantys mokiniai", lessons, NL);
    displayResults(FO);
    FO.close();

 return 0; }

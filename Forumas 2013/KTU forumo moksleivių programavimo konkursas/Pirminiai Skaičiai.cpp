#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>

using namespace std;

    vector<int> primes;
    int fibMemo[1000];

    int fib(int n){
        if(fibMemo[n] != -1) return fibMemo[n];
        if(n <= 2) return fibMemo[n] = 1;
        return fibMemo[n] = fib(n - 1) + fib(n - 2);
    }

    void Esieve(int n){

        bool arr[n + 1];
        for(int i = 0; i <= n; i++)
            arr[i] = 1;
        arr[0] = false;
        arr[1] = false;

        for(int i = 2; i <= n; i++){
            for(int j = 2; i * j <= n; j++){
                if(arr[i * j]) arr[i * j] = false;
            }
        }
        for(int i = 0; i <= n; i++)
            if(arr[i]) primes.push_back(i);

    }

    int reverseInt(int n){
        int r = 0;
        while(n > 0){
            int j = n % 10;
            r = r * 10 + j;
            n /= 10;
        }
    }

    bool fibPrime(int n, int l){
        int rev = reverseInt(n);
        for(int k = 0; k < l; k++){
            for(int i = 0; i < 1000; i++){ // fib
                for(int j = 0; j < primes.size(); j++){ // prim

                }
            }
        }
    }

 int main(){
    int n = 15;
    for(int i = 0; i <= n; i++) fibMemo[i] = -1;

    Esieve(15);
    for(int i = 0; i < primes.size(); i++)
        cout << primes.at(i) << endl;

 return 0; }

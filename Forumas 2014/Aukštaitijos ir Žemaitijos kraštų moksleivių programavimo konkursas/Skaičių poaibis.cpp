#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>

using namespace std;

    int n;
    vector<int> tmpV;

    bool search(int arr[], int l, int b){
        tmpV.clear();
        for(int i = 0; i < l; i++){
            for(int j = i + 1; j < l; j++){
                int sum = 0; sum += arr[i];
                tmpV.push_back(arr[i]);
                for(int k = i + 1; k <= j; k++){
                    sum += arr[k];
                    tmpV.push_back(arr[k]);
                }
                if(sum == b) return true;
                tmpV.clear();
            }
        }
    }

    void coutVector(){
        for(int i = 0; i < tmpV.size(); i++)
            cout << tmpV.at(i) << " ";
        cout << endl;
    }

 int main(){

    ifstream fi("U1.txt");
    fi >> n;
    for(int i = 0; i < n; i++){
        int a, b; fi >> a >> b;
        int arr[b];
        for(int j = 0; j < b; j++) fi >> arr[j];
        bool can = search(arr, b, a);
        cout << i + 1 << " - "; if(!can) cout << "negalima" << endl; else { cout << "galima" << endl; coutVector(); }
    }
   fi.close();

 return 0; }

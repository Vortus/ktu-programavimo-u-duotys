#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    #define FILE "U1.txt"

    bool CheckSubarray(int *a, int *n, int *s, int &i, int &j){
        int tmpsum;
        for(i = 0; i < *n; i++){
            tmpsum = 0;
            for(j = i; j < *n && tmpsum < *s; j++)
                tmpsum += *(a + j);
            if(tmpsum == *s){ j--; return true; }
        }
        return false;
    }

 int main(){

    int N, i, j, k, a, b, x, y;
    bool found;
    ifstream FI(FILE); FI >> N;

    for(i = 0; i < N; i++){
        FI >> a >> b;
        int tmpArray[b];
        for(j = 0; j < b; j++) FI >> tmpArray[j];

        if(CheckSubarray(tmpArray, &b, &a, x, y)){
            cout << i + 1 << " - Galima" << endl;
            for(k = x; k <= y; k++)
                cout << tmpArray[k];
            cout << endl;
        }else{
            cout << i + 1 << " - Negalima" << endl;
        }

    }

    FI.close();


 return 0; }

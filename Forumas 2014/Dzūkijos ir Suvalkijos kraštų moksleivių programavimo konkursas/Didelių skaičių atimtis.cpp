#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;


    string subtract(string a, string b){

        int bl = b.length();
        int diff = a.length() - bl;
        int remainder = 0;
        for(int i = bl - 1; i >= 0; i--){
            int an = a[i + diff] - '0', bn = b[i] - '0';
            an -= remainder; remainder = 0;
            int j = an - bn;
            if(j < 0){ remainder++; j += 10; }
            a[i + diff] = j + '0';
        }

        int i = diff - 1;
        while(remainder != 0){
            int an = a[i] - '0';
            an -= remainder; remainder = 0;
            if(an < 0){ remainder++; an += 10; }
            a[i] = an + '0';
            i--;
        }
        return a;
    }

 int main(){

    ifstream fi("U2.txt");
    int n; fi >> n;

    for(int i = 0; i < n; i++){
        string a, b; fi >> a >> b;
        cout << subtract(a, b) << endl;
    }

    fi.close();


 return 0; }

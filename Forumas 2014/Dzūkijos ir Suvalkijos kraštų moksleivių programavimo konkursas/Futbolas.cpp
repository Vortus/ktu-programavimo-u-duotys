#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

    int n; // zaideju kiekis

    struct Point{
        int x, y;
    };

    struct Player{
        int minutes, nr;
        double distance = 0;
        Point points[92];
    } players[21];


    double dist(int x, int y, int xx ,int yy){
        return sqrt((xx - x) * (xx - x) + (yy - y) * (yy - y));
    }

    void fastest(){
        int index = 0;
        double speed = players[index].distance / players[index].minutes;
        for(int i = 1; i < n; i++)
            if(players[i].distance / players[i].minutes > speed){ speed = players[i].distance / players[i].minutes; index = i; }
        cout << players[index].nr << " " << fixed << setprecision(2) << players[index].distance / players[index].minutes << " m/min" << endl;
    }

    void mostDistance(){
        int index = 0;
        double dist = players[index].distance;
        for(int i = 1; i < n; i++)
            if(players[i].distance > dist){ dist = players[i].distance; index = i; }
        cout << players[index].nr << " " << fixed << setprecision(2) << players[index].distance << " m" << endl;
    }

    void readFile(){
        ifstream fi("U1.txt");
        fi >> n;
        for(int i = 0; i < n; i++){
            fi >> players[i].nr;
            int j; fi >> j;
            players[i].minutes = j - 1;
            for(int k = 0; k < j; k++){
                int x, y; fi >> x >> y;
                players[i].points[k].x = x;
                players[i].points[k].y = y;
                if(k != 0){
                    players[i].distance +=
                        dist(players[i].points[k - 1].x,
                             players[i].points[k - 1].y,
                             x, y);
                }
            }
        }
        fi.close();
    }

 int main(){

    readFile();
    fastest();
    mostDistance();

 return 0; }

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string.h>

using namespace std;

    struct Word{
        int nr, x, y;
        string text;
    } words[1000];

    int w, h, n; // plotis aukstis boardo, kiekis
    char board[101][101];
    int explored[101][101];
    int resultBoard[101][101];

    bool explore(int x, int y, Word word, int index){
        if(explored[y][x] == 1 || word.text[index] != board[y][x]) // jeo netinka
            return false;

        if(word.text.length() - 1 == index) { // jei rado tinkama
            resultBoard[y][x] = word.nr;
            explored[y][x] = 1;
            return true;
        }
        explored[y][x] = 1; // praexplorini

        if(x != 0 && explore(x - 1, y, word, index + 1)){ resultBoard[y][x] = word.nr; // explorinii kare
        return true;}
        if(y != 0 && explore(x, y - 1, word, index + 1)){ resultBoard[y][x] = word.nr; // i virsu
        return true;}
        if(x != w - 1 && explore(x + 1, y, word, index + 1)){ resultBoard[y][x] = word.nr; // i desine
        return true;}
        if(y != h - 1 && explore(x, y + 1, word, index + 1)){ resultBoard[y][x] = word.nr; // i apcacia
        return true;}

        explored[y][x] = 0; // jei pusiaukelej tai paresetinti

        return false;
    }

    void readFile(){
        ifstream fi("U3.txt");
        fi >> h >> w;
        for(int i = 0; i < h; i++)
            for(int j = 0; j < w; j++){
                fi >> board[i][j];
                explored[i][j] = 0;
            }
        fi >> n;
        for(int i = 0; i < n; i++){
            words[i].nr = i + 1;
            fi >> words[i].text;
            fi >> words[i].y >> words[i].x;
            words[i].x--; words[i].y--;
        }
        fi.close();
    }

 int main(){

    readFile();

    for(int i = 0; i < n; i++) // praeksplorint visus zodzius
        explore(words[i].x, words[i].y, words[i], 0);

    for(int i = 0; i < h; i++){ // boardas
        for(int j = 0; j < w; j++)
                cout << setw(4) << resultBoard[i][j];
        cout << endl;
    }

    for(int i = 0; i < w; i++){ // suma
            int sum = 0;
            for(int j = 0; j < h; j++)
                sum += resultBoard[j][i];
        cout << setw(4) << sum;
    }

 return 0; }

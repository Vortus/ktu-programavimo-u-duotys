#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    void sortHorizontalLine(int arr[]){
     for(int y = 0; y < 3; y++){
        for(int i = 0; i < 3; i++){
            for(int j = i + 1; j < 3; j++){
                if(arr[i + y * 3] > arr[j + y * 3]){
                    int tmp = arr[i + y * 3];
                    arr[i + y * 3] = arr[j + y * 3];
                    arr[j + y * 3] = tmp;
                }
            }
        }
      }
    }

    void sortVerticalLine(int arr[]){
        for(int x = 0; x < 3; x++){
        for(int i = 0; i < 3; i++){
            for(int j = i + 1; j < 3; j++){
                if(arr[x + i * 3] < arr[x + j * 3]){
                    int tmp = arr[x + i * 3];
                    arr[x + i * 3] = arr[x + j * 3];
                    arr[x + j * 3] = tmp;
                }
            }
           }
        }

    }

    int v(int arr[]){
        int result = 1;
        for(int i = 0; i < 3; i++)
            result *= arr[i];
        return result;
    }

    bool check(int arr[], int s){
        for(int i = 8; i >= 3; i--)
            if(arr[i] + s * s > arr[i - 3]) return false;
        return true;
    }

    void coutArr(int arr[], int l){
        for(int i = 0; i < l; i++){
            if(i != 0 && i % 3 == 0) cout << endl;
            cout << arr[i] << " ";
        }
        cout << endl << "-----------------" << endl;
    }

 int main(){

    ifstream fi("U1.txt");
    int n, s; fi >> n >> s;
    int allv = 0; // visas turis
    int goodtrio = 0; // geri trigubi

    for(int i = 0; i < n / 3; i++){
        int arr[9];
        for(int j = 0; j < 9; j++) fi >> arr[j];
        sortHorizontalLine(arr);
        sortVerticalLine(arr);
        bool b = check(arr, s);
        coutArr(arr, 9);
        if(b){ goodtrio++; allv += v(arr); }
    }
    fi.close();

    cout << goodtrio << " " << allv << " " << n / 3 - goodtrio;


 return 0; }

#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>

using namespace std;


    vector<int> primes;

    void eSieve(int max){
        int arr[max];
        for(int i = 0; i < max; i++) arr[i] = 1;
        arr[0] = 0; arr[1] = 0;

        for(int i = 2; i < max; i++){
            for(int j = 2; j * i < max; j++){
                arr[j * i] = false;
            }
        }
        for(int i = 0; i < max; i++)
            if(arr[i] == 1) primes.push_back(i);
    }

 int main(){

    eSieve(1000);

	ifstream fi("U2.txt");
    int x, y; fi >> x >> y;

    for(int i = 0; i < y; i++){
        int j; fi >> j;
        int index = 0, sand = 1, offset = 0;
        string s;
        while(j != sand){
            if(sand > j){ offset++; index = 0; sand = 1; s = ""; }
            int p = primes.at(offset + index);
            char c = (offset + index + 1) + '0'; s += c; s+= " ";
            sand *= p;
            index++;
        }
        cout << s << endl;
    }

	fi.close();

 return 0; }

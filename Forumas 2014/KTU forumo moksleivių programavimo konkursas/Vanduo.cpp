#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <algorithm>
#include <queue>
using namespace std;

/*
    Grafai, BFS, Referencai.
*/

    #define MAXN 25
    #define MAXM 25

    struct Transition{ // perejimas vamzdziu
        int connectionA, connectionB, height;
        bool operator <(const Transition &p) const{
            return height > p.height;
        }
    };

    struct VPipe{
        int water, lastHeight = 9999, ID;
    };

    int N, M, K, V; // vertikaliu , horizontaliu, i kuri pilam, kiek pilam
    VPipe pipes[MAXN]; // vertikaliu
    Transition transitions[MAXM]; // horizontaliu

    inline int other(int a, int b, int w){
        return a == w ? b : a;
    }

    inline bool is(int a, int b, int w){
        return a == w || b == w;
    }

    void readFile(){ // failo nuskaitymas
        ifstream fi("U3.txt");
        fi >> N >> M >> K >> V;
        int a, b, c; Transition tmp;
        for(int i = 1; i <= M; i++){
            fi >> a >> b >> c;
            tmp.connectionA = a; tmp.connectionB = b; tmp.height = c;
            pipes[a].ID = a; a; pipes[b].ID = b;
            transitions[i] = tmp;
        }
        sort(transitions + 1, transitions + M + 1);
        fi.close();
    }

    void exploreBFS(int pipe){
        queue<int> q; q.push(pipe);
        while(!q.empty()){
            pipe = q.front(); q.pop();
            for(int i = 1; i <= M; i++){ // per visus galimus transitionus
                Transition *t = &transitions[i];
                bool b = is(t -> connectionA, t -> connectionB, pipes[pipe].ID); // jei yra perejimas
                int h = pipes[pipe].lastHeight;
                int hh = t -> height;
                if(b && hh <= h && hh != -1){
                    t -> height = -1;
                    int j = other(t -> connectionA, t -> connectionB,  pipes[pipe].ID); // kitas indekasa
                    int jj = pipes[pipe].water / 3;
                    pipes[pipe].water -= jj;
                    pipes[j].water += jj;
                    q.push(j);
                }
            }
        }
    }

 int main(){

    readFile();
    pipes[K].water = V;
    exploreBFS(K);

    for(int i = 1; i <= N; i++)
        cout << i << ": " << pipes[i].water << endl;

 return 0; }

#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    int travel[20][60];
    int tlength, friends, dayc;

    void readFile(string s){  // failo nuskaitymas
        ifstream fi(s);
        fi >> tlength >> friends >> dayc;
        for(int j = 0; j < friends; j++)
            for(int i = 0; i < dayc; i++)
                fi >> travel[j][i];

        fi.close();
    }

    int verticalCheck(int x){ // kiek 0 vienam stulpely
        int result = 0;
        for(int i = 0; i < friends; i++){
            if(travel[i][x] == 0) result++;
        }
        return result;
    }

    int largestStart(){ // nuo kur pradeda geriausiai
        int max = 0, maxI = 0;
        for(int i = 0; i <= dayc - tlength; i++){
            int sum = 0;
            for(int j = 0; j < tlength; j++)
                sum += verticalCheck(i + j);
            if(sum > max){ max = sum; maxI = i; }
        }
        return maxI;
    }

    int crossUnwanted(bool arr[], int startI){ // istrina kurie netinka
        int tmp = 0, count = 0; // count geri viariantai
        for(int y = 0; y < friends; y++){
            int tmps = 0;
            for(int x = startI; x < startI + tlength; x++)
                tmps += travel[y][x];
            if(tmps > 0){ arr[tmp] = false; tmp++; }
            else { arr[tmp] = true; tmp++; count++; }
        }
        return count;
    }

    void returnToScreen(){ // atgrazina i ekrana
        ////kiek galimu draugu
        int startI = largestStart();
        bool friendsAvailable[friends];
        int c = crossUnwanted(friendsAvailable, startI); cout << c << endl;
        ////galimi draugai
        if(c != friends){
            for(int i = 0; i < friends; i++)
                if(friendsAvailable[i]) cout << i << " ";
            cout << endl;
        }
        //kalendorius
        for(int i = 0; i < dayc; i++)
            if(i >= startI && i < startI + tlength) cout << 1 << " ";
            else cout << 0 << " ";

    }

 int main(){

    readFile("U3.txt");
   // returnToScreen();

 return 0; }

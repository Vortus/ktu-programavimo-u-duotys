#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>

using namespace std;

    int w = 30;
    int h = 12;

    struct TempLinear{
        double average;
        vector<int> nums;
    };

    vector<TempLinear> allTemps; // dinaminis masyvas visu galimu variantu

    void readFile(double arr[], string s){
        ifstream fi(s);
        fi >> h;

        for(int y = 0; y < h; y++)
            for(int x = 0; x < 30; x++)
                fi >> arr[x + y * w]; // vienmatis kaip dvimatis

        fi.close();
    }

    void printVectorNums(vector<int> nums){
        for(int i = 0; i < nums.size(); i++)
            cout << nums.at(i) << " ";
        cout << endl;
    }

    void findMaxCout(){
        double maxAv = 0, maxI = 0;
        for(int i = 0; i < allTemps.size(); i++){
            if(maxAv < allTemps.at(i).average){ maxAv = allTemps.at(i).average; maxI = i; }
        }
        printVectorNums(allTemps.at(maxI).nums);
    }

 int main(){

    double temps[w * h];
    readFile(temps, "U2.txt");

   for(int y = 0; y < h; y++){
    for(int i = 0; i < w; i++){
        for(int j = i + 1; j < w; j++){
            double average = 0;
            TempLinear tmp;

            for(int k = i; k <= j; k++){
                average += temps[k + y * w];
                tmp.nums.push_back((int)temps[k + y * w]);
            }
            tmp.average = average / tmp.nums.size();
            allTemps.push_back(tmp);
        }
    }
       findMaxCout();
   }

 return 0; }

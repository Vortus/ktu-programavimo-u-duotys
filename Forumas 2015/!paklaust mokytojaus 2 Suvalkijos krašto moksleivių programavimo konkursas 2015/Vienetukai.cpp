#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int oneCount(int n){
        int r = 0;
        while(n > 0){
            int j = n % 2;
            if(j == 1) r++;
            n /= 2;
        }
        return r;
    }

    int maxOneCount(int a, int b){
        int maxOnes = 0;
        for(int i = a; i <= b; i++){
            int j = oneCount(i);
            if(maxOnes < j) maxOnes = j;
        }
        return maxOnes;
    }

 int main(){

    ifstream fi("U1.txt");
    int n; fi >> n;

    for(int i = 1; i <= n; i++){
        int j; fi >> j;
        int maxs = maxOneCount(1, j);
        for(int d = 1; d <= j; d++)
            if(oneCount(d) == maxs) cout << d  << " ";
        cout << endl;
    }

    fi.close();

 return 0; }

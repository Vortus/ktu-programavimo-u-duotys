#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    struct Exercise{
        int index = -1;
        int points[3]; // trieju vertintoju taskai
        double average;
    };

    bool simpleExercise(Exercise arr[], int minArr[], int maxArr[], int i){
        int minc = 0, maxc = 0;
            for(int j = 0; j < 3; j++)
                if(arr[i].points[j] == minArr[j]) minc++;
                else if(arr[i].points[j] == maxArr[j]) maxc++;
        if(minc == 2 && maxc == 0) return true;
        return false;
    }

 int main(){

    ////////////////Failo nuskaitymas
    ifstream fi("U2.txt");
    int n; fi >> n;
    Exercise arr[n]; int minPoints[3], mostPoints[3];
    for(int i = 0; i < 3; i++){ // trys vertintoai
        int mins = 29999999, maxs = 0;
        for(int j = 0; j < n; j++){
            int d; fi >> d;
            if(arr[j].index == -1) arr[j].index = i;
            arr[j].points[i] = d;
            if(mins > d) mins = d;
            if(maxs < d) maxs = d;
        }
        minPoints[i] = mins;
        mostPoints[i] = maxs;
    }
    fi.close();
    ///////////////////////////

    cout << "Paprasti uzdaviniai: " << endl;
    for(int i = 0; i < n; i++)
      if(simpleExercise(arr, minPoints, mostPoints, i)) cout << i + 1 << " ";

 return 0; }

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <fstream>
#include <vector>
using namespace std;

    #define MAXB 100000

    struct Party{
        int size;
        int members[9999];
    };
    vector<int> buffer(MAXB, 0);
    int n, N, tmp = 0;

    void selectParlament(Party arr[], int n){
        for(int i = 0; i < n; i++)
            for(int j = 0; j < arr[i].size; j++) buffer[arr[i].members[j]]++;

        cout << "Parlamenta sudaro:" << endl;
        bool found = false;
        while(!found){
            for(int i = 0; i < MAXB; i++){
                if(buffer[i] == n) { found = true; cout << i << " "; if(n == N) break; }
            }
            if(found) break;
            else n--;
        }
    }

    void coutParties(Party arr[], int n){
        cout << "Pradiniai surikiuoti duomenys:" << endl;
        for(int i = 0; i < n; i++){
            for(int j = 0; j < arr[i].size; j++)
                cout << arr[i].members[j] << " ";
            cout << endl;
        }
    }


 int main(){

    /////Failo nuskaitymas
    ifstream fi("U3.txt"); fi >> n; N = n;
    Party parties[n];
    for(int i = 0; i < n; i++){
        int j; fi >> j;
        parties[i].size = j;
        for(int k = 0; k < j; k++){
            int c; fi >> c;
            parties[i].members[k] = c;
        }
       sort(parties[i].members, parties[i].members + j);
    }
    fi.close();
    /////////////////////
    coutParties(parties, n);
    selectParlament(parties, n);

 return 0; }

#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
using namespace std;

    #define MAXN 1000
    #define FILE "U3.txt"

    /* Bruteforcinis varijants */

    struct Road{
        string starts, ends;
        int length;
    };

    int N; // keliu skaicius
    string s, e; // pradzia, pabaiga
    vector<Road> roads(MAXN); // keliai

    void readFile(){ // failo nuskaitymas
        ifstream FI(FILE); FI >> N;
        string a, b; int i = 0, j;
        FI >> s >> e;
        while(FI >> a >> b >> j){
            roads[i].starts = a;
            roads[i].ends = b;
            roads[i++].length = j;
        }
        FI.close();
    }

    void explore(int r, string path, int l){
        if(roads[r].ends == e){ path += " " + e; cout << path << " " << l << endl; return; } // jei rado pabaiga
        for(int i = 0; i < N; i++) // eina per visus
            if(r != i && roads[i].starts == roads[r].ends) explore(i, path + " " + roads[i].starts, l + roads[i].length);
    }


 int main(){

    readFile();
    for(int i = 0; i < N; i++) // eina per visus
        if(roads[i].starts == s) explore(i, roads[i].starts, roads[i].length);


 return 0; }

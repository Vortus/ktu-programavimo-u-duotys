#include <iostream>
#include <iomanip>
#include <fstream>
#include <string.h>

using namespace std;

    struct Road{
        char start[11], finish[11];
        int length;
    } roads[50];

    int n, m;
    char start[11], finish[11], startTmp[11];

    string addCharAr(string s, char arr[], int l){
        for(int i = 0; i < l; i++)
            s += arr[i];
        s += '\n';
        return s;
    }

    void readFile(string s){
        ifstream fi(s); fi >> n >> m;
        for(int i = 0; i < n; i++) { string j; fi >> j; } fi.ignore(2); // ignorina 2 newlines nes pavizdy yra
        fi.get(start, 11); fi.get(finish, 11); fi.ignore('\n');
        strcpy(startTmp, start);
        for(int i = 0; i < m; i++){
            char st[11]; fi.get(st, 11);
            char en[11]; fi.get(en, 11);
            int j; fi >> j; fi.ignore();
            strcpy(roads[i].start, st);
            strcpy(roads[i].finish, en);
            roads[i].length = j;
        }
        fi.close();
    }

    void sortRoads(){
        for(int i = 0; i < m; i++)
            for(int j = i + 1; j < m; j++)
                if(roads[i].length > roads[j].length){
                    Road tmp = roads[i];
                    roads[i] = roads[j];
                    roads[j] = tmp;
                }
    }

    string shortestAround(int &l){
        int c = 0;
        string result;
        while(strcmp(start, finish) != 0){
            if(c*c > m) break;
            for(int i = 0; i < m; i++){
                if(strcmp(start, roads[i].start) == 0){
                    l += roads[i].length;
                    result = addCharAr(result, start, 10);
                    strcpy(start, roads[i].finish);
                    break;
                }
            }
            c++;
        }
        result = addCharAr(result, finish, 10);
        return result;
    }

    bool shortestStraight(int &l){
        for(int i = 0; i < m; i++)
            if(strcmp(start, roads[i].start) == 0 && strcmp(finish, roads[i].finish) == 0) { l = roads[i].length; break; }
    }

 int main(){

    readFile("U3.txt");
    sortRoads();
    int shortest = 0; // atstumas tiesioginis
    bool is = shortestStraight(shortest);
    int pathAroundLength = 0;
    string pathAround = shortestAround(pathAroundLength);

    if(is && shortest < pathAroundLength){
        cout << "Minimalus atstumas tarp vietoviu" << endl;
        cout << startTmp << "ir " << finish << shortest << " km" << endl;
        cout << "Trasa eina per vietoves: " << endl;
        cout << startTmp << endl << finish;
        return 0;
    }
        cout << "Minimalus atstumas tarp vietoviu" << endl;
        cout << startTmp << "ir " << finish << pathAroundLength << " km" << endl;
        cout << "Trasa eina per vietoves: " << endl;
        cout << pathAround;

 return 0; }

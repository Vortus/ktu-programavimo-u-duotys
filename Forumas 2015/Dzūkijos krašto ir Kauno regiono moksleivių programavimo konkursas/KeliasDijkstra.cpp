#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
using namespace std;

    #define FILE "U3.txt"
    #define MAXN 12
    #define MAXM 55
    #define INF 2000000000

    /* Dijkstros sprendimas */

    struct Road{
        int length, to;
        Road(int to, int length) : to(to), length(length){}
    };

    struct Place{
        int ID, minL, last; bool visited;
        string name, minPath;
        vector<Road*> roads;
    };

    Place *startPlace, *endPlace; // pradzios vietove
    vector<Place> places(MAXN);
    int N, M; string s, e;

    Place *findPlace(string name){
        for(int i = 0; i < N; i++)
            if(places[i].name == name) return &places[i];
    }

    void readFile(){
        vector<Road> roads;
        ifstream FI(FILE); FI >> N >> M;
        for(int i = 0; i < N; i++){
            places[i].ID = i;
            places[i].minL = INF;
            places[i].last = -1;
            FI >> places[i].name;
        }
        FI >> s >> e;
        string a, b; int l;
        for(int i = 0; i < M; i++){
            FI >> a >> b >> l;
            Place *aa = findPlace(a), *bb = findPlace(b);
            roads.push_back(Road(bb -> ID, l));
            roads.push_back(Road(aa -> ID, l));
        }
        FI.close();
        for(int i = 0; i < M; i++){
            int a = roads[i * 2].to, b = roads[i * 2 + 1].to;
            places[a].roads.push_back(&roads[i * 2 + 1]);
            places[b].roads.push_back(&roads[i * 2]);
        }
        //pradzios vietove
        for(int i = 0; i < N; i++){ if(places[i].name == s){ startPlace = &places[i]; break; } }
    }

    void exploreDijkstra(Place *p){
        p -> minL = 0; p -> visited = true;
        vector<Place*> q; q.push_back(p);
        while(q.size() < N){
            int minLen = INF, minID = -1;
            for(int i = 0; i < q.size(); i++){
                if(q[i] -> ID == startPlace -> ID) return;
                for(int j = 0; j < q[i] -> roads.size(); j++){
                    int indexTo = q[i] -> roads[j] -> to;
                    int toCost = q[i] -> roads[j] -> length;
                    if(places[indexTo].visited) continue;
                    if(q[i] -> minL + toCost <= places[indexTo].minL){
                        places[indexTo].minL = q[i] -> minL + toCost;
                        if(minLen > places[indexTo].minL){
                            minLen = places[indexTo].minL;
                            minID = indexTo;
                            places[minID].last = q[i] -> ID;
                        }
                    }
                }
            }
            if(minID != -1){
                places[minID].visited = true;
                q.push_back(&places[minID]);
            }
        }
    }

 int main(){

    readFile(); endPlace = findPlace(e);
    exploreDijkstra(endPlace);
    cout << startPlace -> minL << endl;

    int i = startPlace -> ID;
    while(i > -1){
        cout << places[i].name << endl;
        i = places[i].last;
    }

 return 0; }

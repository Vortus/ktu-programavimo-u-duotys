#include <iostream>
#include <iomanip>
#include <fstream>
#include <map>

using namespace std;

    char alphabet[23] { 'A', 'B', 'C', 'D', 'E', 'F', 'G',
                        'H', 'I', 'Y', 'J', 'K', 'L', 'M',
                        'N', 'O', 'P', 'R', 'S', 'T', 'U',
                        'V', 'Z' };

    map<char, char> encoder;
    string keyword;
    int n;
    string words[10];

    void readFile(string s){
        ifstream fi(s);
        fi >> keyword;
        fi >> n;
        for(int i = 0; i < n; i++)
            fi >> words[i];
        fi.close();
    }

    bool contains(string s, char c){
        for(int i = 0; i < s.length(); i++)
            if(s[i] == c) return true;
        return false;
    }

    string encryptor(){
        string result; result += keyword;
        for(int i = 0; i < 23; i++)
            if(!contains(keyword, alphabet[i])) result += alphabet[i];
        return result;
    }

    void setMap(string s){
        for(int i = 0; i < 23; i++)
            encoder[alphabet[i]] = s[i];
    }

    string encrypt(string s){
        for(int i = 0; i < s.length(); i++)
            s[i] = encoder[s[i]];
        return s;;
    }

    void coutEncryptedWords(){
        for(int i = 0; i < n; i++){
            cout << encrypt(words[i]);
            cout << endl;
        }
    }

 int main(){

    readFile("U2.txt");
    setMap(encryptor());
    coutEncryptedWords();

 return 0; }

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
# define M_PI           3.14159265358979323846

using namespace std;

    double lengthBetween(double x1, double y1, double x2, double y2){
        return sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
    }

    double fx, fy, fr, flimit;//varles
    double zx, zy;//ziogp

    bool check(double dist, double limit, double &l){
        if(dist <= limit){ l++; return true; }
        return false;
    }

 int main(){

    ifstream fi("U1.txt");
    fi >> fx >> fy >> fr >> flimit;
    fi >> zx >> zy; flimit = (fr + flimit) / 2;

    int n; double l = 0; fi >> n;
    for(int i = 0; i < n; i++){
        double a, b; fi >> a >> b;
        double tmpx = zx + a;
        double tmpy = zy + b;
        cout << check(lengthBetween(tmpx, tmpy, zx, zy), flimit, l) << endl;
    }
        cout << l;

    fi.close();

 return 0; }

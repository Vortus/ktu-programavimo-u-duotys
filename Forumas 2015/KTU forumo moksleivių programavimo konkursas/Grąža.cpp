#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    void sortDescending(int arr[], int l){
        for(int i = 0; i < l; i++)
            for(int j = i + 1; j < l; j++)
                if(arr[i] < arr[j]){
                    int tmp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = tmp;
        }
    }

    void resetArr(int arr[], int l){
        for(int i = 0; i < l; i++) arr[i] = 0;
    }

    void coutArr(int arr[], int l, int nominals[]){
        for(int i = 0; i < l; i++)
            if(arr[i] != 0) cout << arr[i] << " " << nominals[i] << " " << endl;
    }

  int main(){

    ifstream fi("U2.txt");
    int n, maxMoney; fi >> n;
    int nominals[n];
    for(int i = 0; i < n; i++) fi >> nominals[i];
    fi >> maxMoney;
    fi.close();
    sortDescending(nominals, n);

    for(int i = 0; i < n; i++){
        int tempArr[n]; resetArr(tempArr, n);
        int tmpMoney = maxMoney;
        for(int j = i; j < n; j++){
            tempArr[j] = tmpMoney / nominals[j];
            tmpMoney %= nominals[j];
        }
        if(tmpMoney == 0) { coutArr(tempArr, n, nominals); return 0; }
    }

 return 0; }

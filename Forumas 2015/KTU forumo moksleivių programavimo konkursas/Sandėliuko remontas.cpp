#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

    double paperCost(int wallH, int wallW, int paperH, int paperW, double cost){
        double a = paperW / wallH;
        double b = a * paperH;
        double c = ceil(wallW / b);
        return c * cost;
    }

 int main(){

    ifstream fi("U1.txt");
    int n; fi >> n;
    string minS;
    double mins = 0x7fffffff; //maxint

    for(int i = 0; i < n; i++){
        int h, w, c; fi >> h >> w >> c;
        for(int j = 0; j < c; j++){
            char s[11]; fi.ignore(); fi.get(s, sizeof s);
            int a, b; fi >> a >> b;//aukstis plotis
            double d, cost; fi >> d;//kaina
            cost = paperCost(h, w, a, b, d);
            if(mins > cost){ mins = cost; minS = s; }
            cout << s << " " << fixed << setprecision(2) << cost << endl;
        }
        cout << "Pigiausi tapetai " << minS << endl;
    }

    fi.close();


 return 0; }

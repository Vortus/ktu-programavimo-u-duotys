#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <cmath>

using namespace std;

    struct Point{
        int x, y;
    };

    int w, h;
    char map[10][10];
    char mapTmp[10][10];

    vector<Point> pizzaPoints;
    vector<Point> stopPoints;
    vector<Point> friendPoints;

    void refreshTmpMap(){
        for(int i = 0; i < h; i++){
            for(int j = 0; j < w; j++)
                mapTmp[i][j] = map[i][j];
        }
    }

    void readFile(string s){
        ifstream fi(s);
        fi >> w >> h;
        for(int y = 0; y < h; y++){
            for(int x = 0; x < w; x++){
                char c; fi.get(c);
                Point tmp;
                tmp.x = x; tmp.y = y;
                if(c == '\n') fi.get(c);
                if(c == 'P'){ pizzaPoints.push_back(tmp); }
                else if(c == 'S'){ stopPoints.push_back(tmp); }
                else if(c == 'D'){ friendPoints.push_back(tmp); }
                map[y][x] = c;
            }
        }
        fi.close();
    }

    bool solveWalking(int &steps, int x, int y, int endX, int endY){ // trumpiausias atstumas labirinte
        if(mapTmp[y][x] == 'X') return false;
        if(x == endX && y == endY) return true;
        mapTmp[y][x] = 'X';// uzlipi
        if(x != 0 && endX < x) if(solveWalking(steps, x - 1, y, endX, endY)){ steps++; return true; }
        if(y != 0 && endY < y) if(solveWalking(steps, x, y - 1, endX, endY)){ steps++;  return true; }
        if(x != w - 1 && endX > x) if(solveWalking(steps, x + 1, y, endX, endY)){ steps++;  return true; }
        if(y != h - 1 && endY > y) if(solveWalking(steps, x, y + 1, endX, endY)){ steps++;  return true; }
        return false;
    }

    Point bestStop(int &minSteps, int &c){ // arciausias stopo taskas
        Point result; minSteps = 299999999;
        for(int i = 0; i < stopPoints.size(); i++){
            int steps = 0;
            for(int j = 0; j < friendPoints.size(); j++){
                refreshTmpMap();
                int x = friendPoints.at(j).x;
                int y = friendPoints.at(j).y;
                bool b = solveWalking(steps, x, y, stopPoints.at(i).x, stopPoints.at(i).y);
                c += b;
            }

            if(minSteps > steps){
                minSteps = steps;
                result.x = stopPoints.at(i).x;
                result.y = stopPoints.at(i).y;
            }
        }

        return result;
    }

    Point bestPizza(Point startP, int &minSteps){
        Point result; minSteps = 299999999;
        for(int i = 0; i < pizzaPoints.size(); i++){
            refreshTmpMap();
            int steps = 0;
            solveWalking(steps, startP.x, startP.y, pizzaPoints.at(i).x, pizzaPoints.at(i).y);
            if(minSteps > steps){
                minSteps = steps;
                result.x = pizzaPoints.at(i).x;
                result.y = pizzaPoints.at(i).y;
            }
        }

        return result;
    }

    void coutFriends(){
        for(int i = friendPoints.size() - 1; i >= 0; i--)
            cout << friendPoints.at(i).x + 1 << " "
                << abs(friendPoints.at(i).y + 1 - h) + 1 << endl;

    }

 int main(){

    readFile("U3.txt");
    int allSteps, c = 0;
    Point best = bestStop(allSteps, c);
    if(c != stopPoints.size() * friendPoints.size()){
        coutFriends();
        cout << "Neimanoma";
        return 0;
    }

    int topizzas;
    Point bestP = bestPizza(best, topizzas);
    allSteps += topizzas * friendPoints.size();
    allSteps *= 2;
    coutFriends();
    cout << "Susitikimo vieta " << best.x + 1 << " " << best.y + 1 << endl;
    cout << "Picerija " << bestP.x + 1<< " " << bestP.y + 1 << endl;
    cout << "Nueita " << allSteps;

 return 0; }

#include <iostream>
#include <iomanip>
#include <fstream>
#include <stack>
#include <cmath>
using namespace std;

    /*
        Sprendimas: naudoti paie�k� gylyn su salyga jog i� kra�to galima
        pereiti � kit� kra�t� kadangi galai yra sujungti.
    */

    #define FILE "U3.txt"
    #define MAXN 35
    int HEIGHT, WIDTH; // Maksimalus auk�tis, plotis lentel�s
    bool BOARD[MAXN][MAXN]; // Visa lentel�

    typedef pair<int,int> Point; // Ta�kas dvimat�je erdv�je

    void readFile(){ // Failo nuskaitymas
        ifstream FI(FILE); FI >> HEIGHT >> WIDTH; // Nuskaitomas lentel�s auk�tis, plotis
        int i, j; // Counteriai
        for(i = 0; i < HEIGHT; i++)
            for(j = 0; j < WIDTH; j++)
                FI >> BOARD[i][j];
        FI.close();
    }

    inline bool validAdvance(int x, int y){ // Ar galimas �jimas
        return x >= 0 && x < WIDTH && y >= 0 && y < HEIGHT;
    }

    void exploreDFS(Point p){ // Ie�kome su paie�ka gylyn
        stack<Point> s; s.push(p); BOARD[p.second][p.first] = 2;
        int x, y, nx, ny;
        while(!s.empty()){
            p = s.top(); s.pop();
            for(y = -1; y <= 1; y++){ // � visas direkcijas
                for(x = -1; x <= 1; x++){
                    if(abs(x) != abs(y)){ // Jei ne �stri�a direkcija
                        nx = p.first + x;
                        ny = p.second + y;
                        if(nx < 0) nx = WIDTH - 1; // Per�okimai nes cilindras
                        if(nx >= WIDTH) nx = 0;
                        if(validAdvance(nx, ny) && BOARD[ny][nx] == 1){ // Jei galima u�eiti
                            s.push(Point(nx, ny));
                            BOARD[ny][nx] = 0; // Aplank�me langel�
                        }
                    }
                }
            }
        }
    }

    int pieceCount(){ // I�ie�koma lentel� visi�kai
        int result = 0;
        for(int y = 0; y < HEIGHT; y++){
            for(int x = 0; x < WIDTH; x++){
                if(BOARD[y][x] == 1){
                    exploreDFS(Point(y, x));
                    result++;
                }
            }
        }
        return result;
    }

 int main(){

    readFile(); // Nuskaitomas failas
    cout << pieceCount(); // I�vedamas rezultatas

 return 0; }
